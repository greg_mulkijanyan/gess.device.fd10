﻿using Gess.Device.Common;

namespace Gess.Device.Mindray.HL7
{
    // ReSharper disable InconsistentNaming
    public enum HL7Parameter
    {
        [PatientParameterValue(PatientParameter.T1)]
        T1 = 200,

        [PatientParameterValue(PatientParameter.T2)]
        T2 = 201,

        [PatientParameterValue(PatientParameter.TD)]
        TD = 202,

        [PatientParameterValue(PatientParameter.NIBPSys)]
        NIBPSys = 170,

        [PatientParameterValue(PatientParameter.NIBPDia)]
        NIBPDia = 171,

        [PatientParameterValue(PatientParameter.NIBPMean)]
        NIBPMean = 172,

        [PatientParameterValue(PatientParameter.SpO2)]
        SpO2 = 160,

        [PatientParameterValue(PatientParameter.PR)]
        PR = 161,

        [PatientParameterValue(PatientParameter.HR)]
        HR = 101,

        [PatientParameterValue(PatientParameter.RR)]
        RR = 151,

        [PatientParameterValue(PatientParameter.CO)]
        CO = 123,

        [PatientParameterValue(PatientParameter.CO2)]
        CO2 = 124
    }
    // ReSharper restore InconsistentNaming
}
