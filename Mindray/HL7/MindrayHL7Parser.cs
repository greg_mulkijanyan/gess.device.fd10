﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Gess.Device.Common;
using Gess.Device.Events;
using Gess.Device.Messages;
using NHapi.Base;
using NHapi.Base.Model;
using NHapi.Base.Parser;
using NHapi.Base.Util;
using NHapi.Model.V231.Datatype;
using NHapi.Model.V231.Message;
using NHapi.Model.V231.Segment;
using DataReceivedEventArgs = Gess.Device.Events.DataReceivedEventArgs;

namespace Gess.Device.Mindray.HL7
{
    /// <summary>
    /// HL7 parser for Mindray devices (iMEC family).
    /// </summary>
    // ReSharper disable InconsistentNaming
    public class MindrayHL7Parser : IPatientDataSource
    // ReSharper restore InconsistentNaming
    {
        #region [ Constructor(s) ]

        /// <summary>
        /// Creates Mindray device parser, given the monitor device connector object.
        /// </summary>
        /// <param name="monitorDevice">Low-level connector object, used to extract raw data from PMD.</param>
        /// <param name="loggerInstance">Logger service object, used to log messages and exceptions.</param>
        public MindrayHL7Parser(IPatientMonitorDevice monitorDevice, ILogger loggerInstance)
        {
            if (monitorDevice == null)
                throw new ArgumentNullException("monitorDevice");

            if (loggerInstance == null) 
                throw new ArgumentNullException("loggerInstance");

            MonitorDevice = monitorDevice;
            MonitorDevice.DataReceived += OnDataReceived;

            logger = loggerInstance;

            UniqueId = new Guid();
        }

        #endregion

        #region [ Private members ]

        private readonly ConcurrentDictionary<PatientParameter, double> upperLimits = new ConcurrentDictionary<PatientParameter, double>();

        private readonly ConcurrentDictionary<PatientParameter, double> lowerLimits = new ConcurrentDictionary<PatientParameter, double>();

        private readonly ConcurrentDictionary<PatientParameter, double> currentValues = new ConcurrentDictionary<PatientParameter, double>();

        private readonly ILogger logger;

        private readonly ConcurrentQueue<AbstractPatientMessage> messageQueue = new ConcurrentQueue<AbstractPatientMessage>();

        private long currentSubscriptionId;

        #region Constants

        // message control IDs
        private const string McidAlarmLimitsChanged = "51";

        private const string McidPhysiologicalAlarm = "54";

        private const string McidTechnicalAlarm = "56";

        private const string McidPatientInformation = "103";
        
        private const string McidEchoResponse = "106";

        private const string McidPatientRealtimeParameters = "204";
        
        private const string McidPatientAperiodicParameters  = "503";

        // alarm limit codes
        private const string AlarmUpperLimit = "2002";

        private const string AlarmLowerLimit = "2003";

        #endregion

        private void RouteMessage(string messageBody)
        {
            if (messageBody == null) 
                return;

            try
            {
                var messageText = messageBody;
                if (messageBody[0] == '\x0b')
                {
                    messageText = messageBody.Substring(1);
                }

                // parse message
                IMessage message = new PipeParser().Parse(FixObrSegment(messageText));

                var r01 = message as ORU_R01;
                if (r01 != null)
                {
                    switch (r01.MSH.MessageControlID.Value)
                    {
                        // parameter limits received initially or changed
                        case McidAlarmLimitsChanged:

                            ProcessMessageORUR01_51(r01);
                            break;

                        // technical alarm
                        case McidTechnicalAlarm:
                            
                            ProcessMessageORUR01_56(r01);
                            break;

                        // physiological alarm
                        case McidPhysiologicalAlarm:
                            
                            ProcessMessageORUR01_54(r01);
                            break;
                        
                        // initial patient information
                        case McidPatientInformation:
                            break;

                        // echo response
                        case McidEchoResponse:
                            break;

                        // real-time patient data
                        case McidPatientRealtimeParameters:
                            ProcessMessageORUR01_204(r01);
                            break;

                        case "205":
                            break;

                        // aperiodic parameter
                        case McidPatientAperiodicParameters:
                            ProcessMessageORUR01_503(r01);
                            break;

                        default:

                            var errorText = String.Format("MessageControlID '{0}' is not supported.", r01.MSH.MessageControlID.Value);
                            var handler = logger.IsRemoteLoggingAvailable() ? (Action<string>) logger.LogRemotely : logger.LogLocally;
                            handler(errorText);
                            break;
                    }
                }
            }
            catch (HL7Exception ex)
            {
                logger.LogException(ex);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
            }
        }

        private static bool NeedsObrSegmentFix(string messageBody)
        {
            if (messageBody == null) 
                throw new ArgumentNullException("messageBody");
            
            return messageBody.Contains("\rOBX|") && !messageBody.Contains("\rOBR|");
        }

        private static string FixObrSegment(string messageBody)
        {
            if (messageBody == null) 
                throw new ArgumentNullException("messageBody");
            
            if (!NeedsObrSegmentFix(messageBody)) 
                return messageBody;

            try
            {
                var messageParts = messageBody.Split(new[] {'\r'}, StringSplitOptions.None);
                var builder = new StringBuilder();

                bool foundObx = false;
                foreach (var s in messageParts)
                {
                    if (s.Substring(0, Math.Min(3, s.Length)) == "OBX" && !foundObx)
                    {
                        foundObx = true;
                        builder.Append("OBR||||Mindray Monitor|||0|\r");
                    }
                    builder.Append(s + '\r');
                }

                return builder.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }

        private AbstractPatientMessage CreateMessageFromHL7(IMessage hl7Message, object eventArgs)
        {
            if (hl7Message == null) 
                throw new ArgumentNullException("hl7Message");

            var oruR01 = hl7Message as ORU_R01;
            if (oruR01 != null)
            {
                switch (oruR01.MSH.MessageControlID.Value)
                {
                    case McidPhysiologicalAlarm :

                        return new PhysiologicalAlarmMessage(eventArgs as PhysiologicalAlarmEventArgs);

                    case McidTechnicalAlarm :

                        return new TechnicalAlarmMessage(eventArgs as TechnicalAlarmEventArgs);

                    case McidAlarmLimitsChanged :
                        
                        return new AlarmLimitChangeMessage(eventArgs as List<AlarmLimitChangeInfo>);

                    case McidPatientAperiodicParameters :
                    case McidPatientRealtimeParameters :
                        
                        return new PatientParametersMessage(eventArgs as PatientStateData);

                    default :
                        return null;
                }
            }

            return null;
        }

        private void TryEnqueue(AbstractPatientMessage message)
        {
            if (message == null) 
                throw new ArgumentNullException("message");

            if (IsSubscribed)
                messageQueue.Enqueue(message);
        }

        #region Message processing

        /// <summary>
        /// Process patient parameters' alarm limits change
        /// </summary>
        /// <param name="message"></param>
        private void ProcessMessageORUR01_51(ORU_R01 message)
        {
            if (message == null) 
                throw new ArgumentNullException("message");

            try
            {
                List<OBX> obxSegments = GetObxSegments(message); // GetSegments<OBX>(message);
                if (obxSegments == null)
                    return;

                var newValues = new Dictionary<Tuple<PatientParameter, ParameterLimitType>, double>();
                var oldValues = new Dictionary<Tuple<PatientParameter, ParameterLimitType>, double>();

                foreach (var obx in obxSegments)
                {
                    string value = obx.GetObservationValue()[0].Data.ToString();
                    string parameter = obx.ObservationSubID.Value;
                    int numericParam;
                    PatientParameter patientParam = PatientParameter.Unknown;

                    if (Int32.TryParse(parameter, out numericParam))
                    {
                        patientParam = EnumHelper.ToPatientParameter((HL7Parameter) numericParam);
                    }

                    Tuple<PatientParameter, ParameterLimitType> key;

                    switch (obx.ObservationIdentifier.Identifier.Value)
                    {
                        case AlarmUpperLimit:

                            key = new Tuple<PatientParameter, ParameterLimitType>(patientParam, ParameterLimitType.Upper);

                            if (newValues.ContainsKey(key))
                            {
                                oldValues[key] = upperLimits[patientParam];
                            }
                            else
                            {
                                oldValues[key] = Double.NaN;
                            }

                            newValues[key] = upperLimits[patientParam] = Double.Parse(value);
                            break;

                        case AlarmLowerLimit:

                            key = new Tuple<PatientParameter, ParameterLimitType>(patientParam, ParameterLimitType.Lower);

                            if (newValues.ContainsKey(key))
                            {
                                oldValues[key] = lowerLimits[patientParam];
                            }
                            else
                            {
                                oldValues[key] = Double.NaN;
                            }

                            newValues[key] = lowerLimits[patientParam] = Double.Parse(value);
                            break;
                    }

                    var linq = from x in newValues
                        let old = oldValues[new Tuple<PatientParameter, ParameterLimitType>(x.Key.Item1, x.Key.Item2)]
                        select new AlarmLimitChangeInfo
                        {
                            Parameter = x.Key.Item1,
                            Type = x.Key.Item2,
                            Value = x.Value,
                            OldValue = old
                        };

                    var alarmLimitsList = linq.ToList();
                    RaiseParameterAlarmLimitChanged(alarmLimitsList);
                    if (IsSubscribed)
                    {
                        var messageToQueue = CreateMessageFromHL7(message, alarmLimitsList);
                        if (messageToQueue != null)
                        {
                            TryEnqueue(messageToQueue);
                        }
                    }
                }
            }
            catch (HL7Exception ex)
            {
                logger.LogException(ex);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
            }
        }

        private void ProcessMessageORUR01_54(ORU_R01 message)
        {
            if (message == null) 
                throw new ArgumentNullException("message");

            try
            {
                var obxSegments = GetObxSegments(message); // GetSegments<OBX>(message);
                if (obxSegments == null || obxSegments.Count == 0) 
                    return;

                foreach (var obx in obxSegments)
                {
                    var args = new PhysiologicalAlarmEventArgs();
                    
                    var alarmLevel = obx.ObservationIdentifier.Identifier.Value;
                    var data = (CE) obx.GetObservationValue()[0].Data;
                    var alarmCode = data.Identifier.Value;
                    var alarmText = data.Text.Value;

                    int parsedAlarmLevel;
                    if (Int32.TryParse(alarmLevel, out parsedAlarmLevel))
                        args.AlarmLevel = parsedAlarmLevel;

                    args.AlarmCode = alarmCode;
                    args.Message = alarmText;
                    var dtString = obx.DateTimeOfTheObservation.TimeOfAnEvent.Value;

                    if (!String.IsNullOrWhiteSpace(dtString))
                    {
                        RaisePhysiologicalAlarmAsync(args);
                        if (IsSubscribed)
                        {
                            var messageToQueue = CreateMessageFromHL7(message, args);
                            if (messageToQueue != null)
                            {
                                TryEnqueue(messageToQueue);
                            }
                        }
                    }
                }
            }
            catch (HL7Exception ex)
            {
                logger.LogException(ex);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
            }
        }

        /// <summary>
        /// Process patient parameters data receive.
        /// </summary>
        /// <param name="message"></param>
        private void ProcessMessageORUR01_204(ORU_R01 message)
        {
            if (message == null) 
                throw new ArgumentNullException("message");

            try
            {
                List<OBX> obxSegments = GetObxSegments(message); // GetSegments<OBX>(message);

                if (obxSegments == null || obxSegments.Count == 0)
                    return;

                var patientData = new PatientStateData();
                
                foreach (var obx in obxSegments)
                {
                    var value = obx.GetObservationValue()[0].Data;
                    string parameter = obx.ObservationIdentifier.Identifier.Value;
                    int numericParam;

                    PatientParameter patientParam;

                    if (Int32.TryParse(parameter, out numericParam))
                    {
                        patientParam = EnumHelper.ToPatientParameter((HL7Parameter) numericParam);
                    }
                    else
                    {
                        logger.LogLocally("Unknown patient parameter received.");
                        continue;
                    }

                    double patientValue;
                    if (Double.TryParse(value.ToString(), out patientValue))
                    {
                        if (!currentValues.TryAdd(patientParam, patientValue))
                        {
                            double cachedValue;
                            currentValues.TryGetValue(patientParam, out cachedValue);

                            if (!currentValues.TryUpdate(patientParam, patientValue, cachedValue))
                            {
                                currentValues[patientParam] = patientValue;
                            }
                        }

                        patientData[patientParam] = patientValue;
                    }
                    else
                    {
                        logger.LogLocally("Invalid patient parameter value.");
                    }
                }

                RaisePatientDataReceivedAsync(patientData);
                
                if (IsSubscribed)
                {
                    var messageToQueue = CreateMessageFromHL7(message, patientData);
                    if (messageToQueue != null)
                    {
                        TryEnqueue(messageToQueue);
                    }
                }
            }
            catch (HL7Exception ex)
            {
                logger.LogException(ex);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
            }
        }

        /// <summary>
        /// Process aperiodic patient parameters data receive.
        /// </summary>
        /// <param name="message"></param>
        private void ProcessMessageORUR01_503(ORU_R01 message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            try
            {
                List<OBX> obxSegments = GetObxSegments(message); // GetSegments<OBX>(message);

                if (obxSegments == null || obxSegments.Count == 0)
                    return;

                var patientData = new PatientStateData();

                foreach (var obx in obxSegments)
                {
                    var value = obx.GetObservationValue()[0].Data;
                    string parameter = obx.ObservationIdentifier.Identifier.Value;
                    int numericParam;

                    PatientParameter patientParam;

                    if (Int32.TryParse(parameter, out numericParam))
                    {
                        patientParam = EnumHelper.ToPatientParameter((HL7Parameter)numericParam);
                    }
                    else
                    {
                        logger.LogLocally("Unknown patient parameter received.");
                        continue;
                    }

                    double patientValue;
                    if (Double.TryParse(value.ToString(), out patientValue))
                    {
                        if (!currentValues.TryAdd(patientParam, patientValue))
                        {
                            double cachedValue;
                            currentValues.TryGetValue(patientParam, out cachedValue);

                            if (!currentValues.TryUpdate(patientParam, patientValue, cachedValue))
                            {
                                currentValues[patientParam] = patientValue;
                            }
                        }

                        patientData[patientParam] = patientValue;
                    }
                    else
                    {
                        logger.LogLocally("Invalid patient parameter value.");
                    }
                }

                RaisePatientDataReceivedAsync(patientData);
                
                if (IsSubscribed)
                {
                    var messageToQueue = CreateMessageFromHL7(message, patientData);
                    if (messageToQueue != null)
                    {
                        TryEnqueue(messageToQueue);
                    }
                }
            }
            catch (HL7Exception ex)
            {
                logger.LogException(ex);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
            }
        }

        /// <summary>
        /// Process technical alarms
        /// </summary>
        /// <param name="message"></param>
        private void ProcessMessageORUR01_56(ORU_R01 message)
        {
            if (message == null) 
                throw new ArgumentNullException("message");

            try
            {
                var obxSegments = GetObxSegments(message); // GetSegments<OBX>(message);
                if (obxSegments == null || obxSegments.Count == 0)
                    return;

                var technicalAlarm = new TechnicalAlarmEventArgs();
                
                foreach (var obx in obxSegments)
                {
                    var alarmLevel = obx.ObservationIdentifier.Identifier.Value;
                    int parsedAlarmLevel;
                    if (Int32.TryParse(alarmLevel, out parsedAlarmLevel))
                    {
                        technicalAlarm.AlarmLevel = parsedAlarmLevel;
                    }
                    else
                    {
                        logger.LogLocally("Invalid alarm level value.");
                        continue;
                    }

                    if (parsedAlarmLevel < 0 || parsedAlarmLevel > 4)
                    {
                        continue;
                    }

                    var observationResults = obx.GetObservationValue()[0].Data as CE;
                    if (observationResults != null)
                    {
                        technicalAlarm.Message = observationResults.Text.Value;
                        int parsedAlarmId;
                        if (Int32.TryParse(observationResults.Identifier.Value, out parsedAlarmId))
                        {
                            technicalAlarm.AlarmId = parsedAlarmId;
                        }
                    }
                }

                RaiseTechnicalAlarmAsync(technicalAlarm);
                if (IsSubscribed)
                {
                    var messageToQueue = CreateMessageFromHL7(message, technicalAlarm);
                    if (messageToQueue != null)
                    {
                        TryEnqueue(messageToQueue);
                    }
                }
            }
            catch (HL7Exception ex)
            {
                logger.LogException(ex);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
            }
        }

        private double TryGetValue(PatientParameter parameter)
        {
            double value;
            return this.currentValues.TryGetValue(parameter, out value) ? value : double.NaN;
        }

        /// <summary>
        /// Checks, if segment is populated or 'empty'.
        /// </summary>
        /// <param name="segment"></param>
        /// <returns></returns>
        private static bool IsEmptySegment(AbstractSegment segment)
        {
            if (segment == null) 
                throw new ArgumentNullException("segment");
            
            var segmentType = segment.GetType();

            if (segmentType == typeof (OBX))
            {
                var obx = segment as OBX;
                if (obx == null) 
                    return false;

                var obxValues = obx.GetObservationValue();
                
                return String.IsNullOrWhiteSpace(obx.ValueType.Value) ||
                       String.IsNullOrWhiteSpace(obx.ObservationIdentifier.Identifier.Value) ||   
                       obxValues == null || obxValues.Length == 0 || obxValues[0] == null;
            }

            return false;
        }

        #endregion

        #endregion

        #region [ Protected members ]

        protected List<OBX> GetObxSegments(ORU_R01 message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            try
            {
                var list = new List<OBX>();
                for (int i = 0; ; i++)
                {
                    try
                    {
                        var obx = message.GetPATIENT_RESULT().GetORDER_OBSERVATION().GetOBSERVATION(i).OBX;
                        if (!IsEmptySegment(obx))
                            list.Add(obx);
                        else
                            break;
                    }
                    catch (HL7Exception)
                    {
                        break;
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                return null;
            }
        }

        /// <summary>
        /// Find all segments of specified type in HL7 message.
        /// </summary>
        /// <typeparam name="T">Type of segment to find, must be descendant of AbstractSegment.</typeparam>
        /// <param name="message">Message to extract segments from.</param>
        /// <returns>List of segments, found in message or null.</returns>
        protected List<T> GetSegments<T>(IMessage message) where T : AbstractSegment
        {
            if (message == null)
                throw new ArgumentNullException("message");

            try
            {
                // init HL7 terser from message
                var terser = new Terser(message);

                // init returned result list
                var output = new List<T>();

                // loop indefinitely, incrementing counter - since some group segments are missing 
                // from most of Mindray messages, we cannot retreieve count of segments of interest.
                for (int i = 0; ; i++)
                {
                    try
                    {
                        // reset terser, to search from start of message
                        terser.Finder.reset();

                        // find requested segment's occurence, identified by loop counter
                        var segment = (T)terser.Finder.findSegment(typeof(T).Name.ToUpper(), i);

                        // check if segment is null - break the loop if so, otherwise add segment to results
                        if (segment != null && !IsEmptySegment(segment))
                            output.Add(segment);
                        else
                            break;
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        Debug.Print(ex.Message + ex.StackTrace);
                        break;
                    }
                    catch (HL7Exception ex)
                    {
                        // break the loop if exception was thrown by terser
                        Debug.Print(ex.Message + ex.StackTrace);
                        break;
                    }
                }

                return output;
            }
            // separate handlers from HL7Exception and others - just 
            // in case we'll need to handle them differently in future.
            catch (HL7Exception ex)
            {
                logger.LogException(ex);
                return null;
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                return null;
            }
        }

        protected virtual void OnDataReceived(object sender, DataReceivedEventArgs args)
        {
            if (sender == null) 
                throw new ArgumentNullException("sender");

            if (args == null) 
                throw new ArgumentNullException("args");

            args.Data.AsParallel().ForAll(RouteMessage);
        }

        protected virtual void RaiseParameterAlarmLimitChanged(List<AlarmLimitChangeInfo> alarmLimitsChanges)
        {
            var handler = this.ParameterAlarmLimitChanged;
            if (handler != null)
            {
                var args = new AlarmLimitChangedEventArgs(alarmLimitsChanges);
                handler(this, args);
            }
        }

        protected virtual void RaiseParameterAlarmLimitChangedAsync(List<AlarmLimitChangeInfo> alarmLimitsChanges)
        {
            var handlers = this.ParameterAlarmLimitChanged; 
            if (handlers == null) 
                return;
            
            foreach (var h in handlers.GetInvocationList().Cast<EventHandler<AlarmLimitChangedEventArgs>>())
            {
                var args = new AlarmLimitChangedEventArgs(alarmLimitsChanges);
                h.BeginInvoke(this, args, null, null);
            }
        }

        protected virtual void RaiseTechnicalAlarm(TechnicalAlarmEventArgs args)
        {
            var handler = this.TechnicalAlarm;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        protected virtual void RaiseTechnicalAlarmAsync(TechnicalAlarmEventArgs args)
        {
            var handlers = this.TechnicalAlarm;
            if (handlers == null) 
                return;
            
            foreach (var h in handlers.GetInvocationList().Cast<EventHandler<TechnicalAlarmEventArgs>>())
            {
                h.BeginInvoke(this, args, null, null);
            }
        }

        protected virtual void RaisePatientDataReceived(PatientStateData patientStateData)
        {
            var handlers = this.PatientDataReceived;
            if (handlers != null)
            {
                handlers(this, patientStateData);
            }
        }

        protected virtual void RaisePatientDataReceivedAsync(PatientStateData patientStateData)
        {
            var handlers = this.PatientDataReceived;
            if (handlers == null) 
                return;

            foreach (var handler in handlers.GetInvocationList().Cast<EventHandler<PatientStateData>>())
            {
                handler.BeginInvoke(this, patientStateData, null, null);
            }
        }

        protected virtual void RaisePhysiologicalAlarm(PhysiologicalAlarmEventArgs args)
        {
            var handlers = this.PhysiologicalAlarm;
            if (handlers != null)
            {
                handlers(this, args);
            }
        }

        protected virtual void RaisePhysiologicalAlarmAsync(PhysiologicalAlarmEventArgs args)
        {
            var handlers = this.PhysiologicalAlarm;
            if (handlers == null) 
                return;
            foreach (var h in handlers.GetInvocationList().Cast<EventHandler<PhysiologicalAlarmEventArgs>>())
            {
                h.BeginInvoke(this, args, null, null);
            }
        }

        #endregion

        #region [ Public members ]

        public IPatientMonitorDevice MonitorDevice { get; protected set; }

        public double GetT1()
        {
            return TryGetValue(PatientParameter.T1);
        }

        public double GetT2()
        {
            return TryGetValue(PatientParameter.T2);
        }

        public double GetTD()
        {
            return TryGetValue(PatientParameter.TD);
        }

        public double Get_ECG_NIBPSis()
        {
            return TryGetValue(PatientParameter.NIBPSys);
        }

        public double Get_ECG_NIBPDia()
        {
            return TryGetValue(PatientParameter.NIBPDia);
        }

        public double Get_ECG_NIBPMean()
        {
            return TryGetValue(PatientParameter.NIBPMean);
        }

        public double Get_SpO2_PR()
        {
            return TryGetValue(PatientParameter.PR);
        }

        public double Get_SpO2()
        {
            return TryGetValue(PatientParameter.SpO2);
        }

        public double Get_ECG_RR()
        {
            return TryGetValue(PatientParameter.RR);
        }

        public double Get_CO()
        {
            return TryGetValue(PatientParameter.CO);
        }

        public double Get_CO2()
        {
            return TryGetValue(PatientParameter.CO2);
        }

        public bool Supports(PatientParameter patientParameter)
        {
            return true;
        }

        private static readonly Dictionary<PatientParameter, string> MeasurementUnits = new Dictionary<PatientParameter, string>
        {
            {PatientParameter.T1, "C"},
            {PatientParameter.T2, "C"},
            {PatientParameter.TD, "C"},
            {PatientParameter.NIBPDia , "mmHg"}, 
            {PatientParameter.NIBPSys , "mmHg"}, 
            {PatientParameter.NIBPMean , "mmHg"}, 
            {PatientParameter.SpO2 , "%"}, 
            {PatientParameter.RR , "rpm"}, 
            {PatientParameter.PR , "bpm"}, 
            {PatientParameter.HR , "bpm"}, 
            {PatientParameter.CO, "L/min"}, 
            {PatientParameter.CO2, "mmHg"}, 
        }; 
        
        public Dictionary<PatientParameter, string> GetMeasurementUnits()
        {
            return MeasurementUnits;
        }

        public bool TryGetValue(PatientParameter parameter, out double value)
        {
            return currentValues.TryGetValue(parameter, out value);
        }

        public double GetUpperLimit(PatientParameter parameter)
        {
            return upperLimits[parameter];
        }

        public double GetLowerLimit(PatientParameter parameter)
        {
            return lowerLimits[parameter];
        }

        public void Reset()
        {
            var count = currentValues.Count;
            var keys = new PatientParameter[count];
            currentValues.Keys.CopyTo(keys, count);

            foreach (var k in keys)
            {
                double d;
                currentValues.TryRemove(k, out d);
            }
        }

        public Guid UniqueId { get; private set; }
        
        public bool Subscribe(out long subscriptionId)
        {
            subscriptionId = 0;
            
            if (IsSubscribed) 
                return false;

            subscriptionId = Environment.TickCount ^ 0x09afbecd18273645L;

            return Interlocked.Add(ref currentSubscriptionId, subscriptionId) == subscriptionId;
        }

        public bool IsSubscribed
        {
            get
            {
                return Interlocked.Read(ref currentSubscriptionId) != 0;
            }
        }

        public bool TryDequeue(long subscriptionId, out AbstractPatientMessage message)
        {
            message = null;
            long current = Interlocked.Read(ref currentSubscriptionId);
            if (current != subscriptionId || current == 0) 
                return false;

            return messageQueue.TryDequeue(out message);
        }

        public void Unsubscribe(long subscriptionId)
        {
            var current = Interlocked.Read(ref currentSubscriptionId);
            if (current == subscriptionId)
                Interlocked.Add(ref currentSubscriptionId, -current);
        }

        #region [ Event(s) ]

        public event EventHandler<PatientStateData> PatientDataReceived = (sender, args) => { };
        
        public event EventHandler<TechnicalAlarmEventArgs> TechnicalAlarm = (sender, args) => { };
        
        public event EventHandler<PhysiologicalAlarmEventArgs> PhysiologicalAlarm = (sender, args) => { };
        
        public event EventHandler<AlarmLimitChangedEventArgs> ParameterAlarmLimitChanged = (sender, args) => { };

        public event EventHandler<AlarmLevelChangedEventArgs> AlarmLevelChanged = (sender, args) => { };

        #endregion

        #endregion
    }
}
