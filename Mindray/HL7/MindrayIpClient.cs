﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Gess.Device.Common;
using Gess.Device.Events;

namespace Gess.Device.Mindray.HL7
{
    public class MindrayIpClient : IPatientMonitorDevice
    {
        #region [ Constructor(s) ]

        public MindrayIpClient() : this(null)
        {

        }

        public MindrayIpClient(ILogger loggerInstance)
        {
            logger = loggerInstance ?? new DummyLogger();
            NetworkChange.NetworkAddressChanged += OnNetworkAddressChanged;
        }

        #endregion

        #region [ Private members ]

        private readonly ILogger logger;

        // ReSharper disable InconsistentNaming
        
        private const string HL7EndOfMessage = "\r\x1c\r";

        private const string HL7EchoMessage = "\x0bMSH|^~\\&|Heckel|FebroData|||||ORU^R01|106|P|2.3.1" + HL7EndOfMessage;

        private const string QueryMessageHeader = "\x0bMSH|^~\\&|Mindray|PDSTest|||||QRY^R02|1203|P|2.3.1\r";
        
        private const string QueryMessageDefinition = "QRD|{0}|R|I|{1}|||||RES\r";
        
        private const string QueryMessageFilter = "QRF|MON||||0&0^1^{0}^0^{1}\r";
        
        // ReSharper restore InconsistentNaming

        private static readonly byte[] MessageEndMarker = { 0x0d, 0x1c, 0x0d }; // "\r\x1c\r"

        private volatile bool endConnection;

        private volatile PatientParameter parameterMask = PatientParameter.T1 | PatientParameter.T2 | PatientParameter.TD |
                                                          PatientParameter.SpO2 | PatientParameter.PR | PatientParameter.RR | PatientParameter.HR |
                                                          PatientParameter.NIBPSys | PatientParameter.NIBPDia | PatientParameter.NIBPMean;

        private volatile int interval;

        private volatile bool parameterMaskChanged;

        private volatile bool intervalChanged;

        private volatile IPEndPoint localEndPoint;

        private volatile NetworkInterface localInterface;

        private readonly ReaderWriterLockSlim rwLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        private long isConnected;

        private volatile int lastPoll;

        private long queryCounter = 1;

        private int connectTimeout;

        private int sendReceiveTimeout;

        private long canRaiseDisconnected;

        /*
        [Obsolete]
        private void ThreadEntryPoint(object endPoint)
        {
            if (endPoint == null)
                throw new ArgumentNullException("endPoint");

            long result = Interlocked.CompareExchange(ref isConnected, 1, 0);
            if (result != 0)
            {
                throw new InvalidOperationException();
            }

            IPEndPoint ipEndPoint = endPoint as IPEndPoint;
            if (ipEndPoint == null)
                throw new ArgumentException();

            bool wasAborted = false;
            endConnection = false;

            try
            {
                using (var tcpClient = new TcpClient())
                {
                    tcpClient.Connect(ipEndPoint);

                    tcpClient.ReceiveTimeout = 5000;

                    localEndPoint = (IPEndPoint) tcpClient.Client.LocalEndPoint;
                    localInterface = GetActiveNetworkInterface(localEndPoint.Address);
                    
                    using (var stream = tcpClient.GetStream())
                    {
                        var buffer = new byte[8192];

                        if (stream.DataAvailable)
                        {
                            int bytesRead;
                            var builder = new StringBuilder();
                            do
                            {
                                bytesRead = stream.Read(buffer, 0, buffer.Length);
                                if (bytesRead > 0)
                                {
                                    builder.Append(Encoding.ASCII.GetString(buffer, 0, bytesRead));
                                }
                            } while (bytesRead == buffer.Length);
                            RaiseDataReceived(builder.ToString());
                        }

                        Thread.Sleep(1000);

                        string initialQuery = BuildQueryMessage(ParametersMask, Interval);
                        byte[] tempBuffer = Encoding.ASCII.GetBytes(initialQuery);
                        stream.Write(tempBuffer, 0, tempBuffer.Length);

                        var waiter = new SpinWait();
                        var tickCount = Environment.TickCount;

                        while (!endConnection)
                        {
                            if (intervalChanged || parameterMaskChanged)
                            {
                                intervalChanged = false;
                                parameterMaskChanged = false;

                                string updatedQuery = BuildQueryMessage(ParametersMask, Interval);
                                byte[] bytesToWrite = Encoding.ASCII.GetBytes(updatedQuery);
                                stream.Write(bytesToWrite, 0, bytesToWrite.Length);
                            }

                            int bytesRead;
                            var builder = new StringBuilder();
                            do
                            {
                                bytesRead = stream.Read(buffer, 0, buffer.Length);
                                if (bytesRead > 0)
                                {
                                    builder.Append(Encoding.ASCII.GetString(buffer, 0, bytesRead));
                                }
                            } while (bytesRead == buffer.Length);
                            RaiseDataReceived(builder.ToString());

                            if (Environment.TickCount - tickCount >= 5000)
                            {
                                byte[] echoBuffer = Encoding.ASCII.GetBytes(HL7EchoMessage);
                                stream.Write(echoBuffer, 0, echoBuffer.Length);
                                tickCount = Environment.TickCount;
                            }

                            waiter.SpinOnce();
                        }
                    }
                }
            }
            catch (ThreadAbortException)
            {
                wasAborted = true;
                Thread.ResetAbort();
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                wasAborted = true;
            }
            finally
            {
                Interlocked.CompareExchange(ref isConnected, 0, 1);
                if (wasAborted)
                {
                    // RaiseDisconnected();
                }
            }
        }
        */
        
        private void OnNetworkAddressChanged(object sender, EventArgs args)
        {
            try
            {
                if (Interlocked.Read(ref isConnected) == 0 || localInterface == null)
                    return;

                // get our NIC
                var nic = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault(i => i.Id == localInterface.Id);

                // return, if wrong NIC or it is up
                if (nic == null || nic.OperationalStatus == OperationalStatus.Up)
                    return;

                // if connected - raise Disconnect event
                if (Interlocked.Read(ref isConnected) != 0)
                    RaiseDisconnected();
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
            }
        }

        private static NetworkInterface GetActiveNetworkInterface(IPAddress address)
        {
            if (address == null) 
                throw new ArgumentNullException("address");

            return (from nic in NetworkInterface.GetAllNetworkInterfaces()
                    where nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet ||
                          nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    let ipInfo = nic.GetIPProperties().UnicastAddresses
                    where ipInfo.Any(t => t.Address.Equals(address)) 
                    select nic).FirstOrDefault();
        }

        private void SocketThread(object endPoint)
        {
            if (endPoint == null)
                throw new ArgumentNullException("endPoint");

            var ipEndPoint = endPoint as IPEndPoint;
            if (ipEndPoint == null)
                throw new ArgumentException("Argument of type 'System.Net.IPEndPoint' was expected.", "endPoint");

            if (Interlocked.CompareExchange(ref isConnected, 1, 0) != 0)
            {
                throw new InvalidOperationException("Device is already connected.");
            }

            // inline helper delegate for writing data to socket
            Action<Socket, string> safeSocketWrite = (socket, text) =>
            {
                if (socket == null) 
                    throw new ArgumentNullException("socket");

                if (text == null)
                    throw new ArgumentNullException("text");

                try
                {
                    var tempBuffer = Encoding.ASCII.GetBytes(text);
                    SocketError error;
                    var bytesSent = socket.Send(tempBuffer, 0, tempBuffer.Length, SocketFlags.None, out error);
                    if (bytesSent == 0 || error != SocketError.Success)
                        // RaiseDisconnected();
                        throw new SocketException((int) error);
                }
                catch (SocketException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    logger.LogException(ex);
                    throw;
                }
            };

            // inline helper delegate for reading data from socket
            Func<Socket, string> safeSocketRead = socket =>
            {
                if (socket == null) 
                    throw new ArgumentNullException("socket");

                try
                {
                    var readBuffer = new byte[socket.ReceiveBufferSize];
                    var readBuilder = new StringBuilder();
                    int bytesReceived;
                    
                    do
                    {
                        SocketError error;
                        bytesReceived = socket.Receive(readBuffer, 0, readBuffer.Length, SocketFlags.None, out error);
                        
                        if (bytesReceived != 0)
                            readBuilder.Append(Encoding.ASCII.GetString(readBuffer, 0, bytesReceived));
                        
                        if (error != SocketError.Success)
                            throw new SocketException((int) error);

                    } while (bytesReceived == readBuffer.Length);

                    return readBuilder.ToString();
                }
                catch (SocketException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    logger.LogException(ex);
                    throw;
                }
            };
            
            try
            {
                using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                {
                    bool isFirstLoop = true;
                    long lastEcho = 0;

                    socket.SendTimeout = socket.ReceiveTimeout = SendReceiveTimeout;
                    
                    // socket.ExclusiveAddressUse = true;
                    // socket.LingerState = new LingerOption(true, 0);

                    Interlocked.CompareExchange(ref canRaiseDisconnected, 1, 0);
                    

                    if (ConnectTimeout <= 0)
                    {
                        socket.Connect(ipEndPoint);
                    }
                    else
                    {
                        AsyncCallback asyncCallback = result =>
                        {
                            if (result == null) 
                                throw new ArgumentNullException("result");
                            
                            var sourceSocket = result.AsyncState as Socket;
                            if (sourceSocket == null) 
                                throw new ArgumentException("Argument of type 'System.Net.Socket' was expected.", "result");

                            if (sourceSocket.Connected && result.IsCompleted)
                                sourceSocket.EndConnect(result);
                        };

                        var asyncResult = socket.BeginConnect(ipEndPoint, asyncCallback, socket);
                        if (!asyncResult.AsyncWaitHandle.WaitOne(ConnectTimeout))
                        {
                            throw new SocketException((int) SocketError.ConnectionAborted);
                        }
                    }

                    // reset it to re-connect
                    endConnection = false;

                    localEndPoint = (IPEndPoint) socket.LocalEndPoint;
                    localInterface = GetActiveNetworkInterface(localEndPoint.Address);
                    
                    // main loop
                    while (!endConnection)
                    {
                        string receivedData;    
                            
                        // receive initial data
                        if (isFirstLoop)
                        {
                            isFirstLoop = false;
                            receivedData = safeSocketRead(socket);

                            if (receivedData != null)
                                RaiseDataReceived(receivedData);

                            // initial query
                            safeSocketWrite(socket, BuildQueryMessage(ParametersMask, Interval));
                        }

                        // updated query with changed parameters and/or queryInterval
                        if (intervalChanged || parameterMaskChanged)
                        {
                            intervalChanged = false;
                            parameterMaskChanged = false;

                            safeSocketWrite(socket, BuildQueryMessage(ParametersMask, Interval));
                        }

                        // receive queried data
                        receivedData = safeSocketRead(socket);
                        if (receivedData != null)
                            RaiseDataReceived(receivedData);

                        // keep-alive
                        lastPoll = Environment.TickCount;
                        if (lastPoll - lastEcho > 5000)
                        {
                            safeSocketWrite(socket, HL7EchoMessage);
                            lastEcho = lastPoll;
                        }
                    }
                }
            }
            catch (SocketException ex)
            {
                logger.LogException(ex);
                RaiseDisconnected();
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                RaiseDisconnected();
            }
            finally
            {
                Interlocked.CompareExchange(ref isConnected, 0, 1);
            }
        }

        internal enum QueryEventType
        {
            Parameter = 1,

            TechnuicalAlarm = 2,

            Non = 0,
        }

        private string BuildQueryMessage(PatientParameter parametersMask, int queryInterval)
        {
            var allFilters = new[]
            {
                PatientParameter.T1, PatientParameter.T2, PatientParameter.TD,
                PatientParameter.NIBPSys, PatientParameter.NIBPDia, PatientParameter.NIBPMean,
                PatientParameter.PR, PatientParameter.HR, PatientParameter.RR,
                PatientParameter.SpO2, PatientParameter.CO, PatientParameter.CO2 
            };

            var selectedFilters = (from f in allFilters
                                   where 0 != (int)(f & parametersMask)
                                   select EnumHelper.ToHL7Parameter(f)).ToList();

            var filterCounter = 0;
            var totalCounter = 0;

            var builder = new StringBuilder();
            var subBuilder = new StringBuilder();

            builder.Append(QueryMessageHeader);
            long queryId = Interlocked.Increment(ref queryCounter);
            builder.AppendFormat(QueryMessageDefinition, DateTime.Now.ToString("yyyyMMddHHmmss"), queryId.ToString("x8"));

            foreach (var filter in selectedFilters)
            {
                totalCounter++;
                filterCounter++;
                if (filterCounter > 5)
                {
                    builder.AppendFormat(QueryMessageFilter, queryInterval, subBuilder);
                    filterCounter = 0;
                    subBuilder.Clear();
                }
                subBuilder.Append((int) filter);
                if (totalCounter % 5 != 0 && totalCounter < selectedFilters.Count)
                {
                    subBuilder.Append("&");
                }
            }
            var subString = subBuilder.ToString();
            if (!String.IsNullOrWhiteSpace(subString))
            {
                builder.AppendFormat(QueryMessageFilter, queryInterval, subString);
            }

            builder.Append("\x1c\r");
            return builder.ToString();
        }
        
        #endregion

        #region [ Protected members ]

        protected static List<int> GetMessageEndMarkers(byte[] buffer)
        {
            int offset = 0;
            int index = -1;

            var output = new List<int>();

            for (var i = 0; i < buffer.Length; i++)
            {
                if (buffer[i] == MessageEndMarker[offset])
                {
                    if (offset == MessageEndMarker.Length - 1)
                    {
                        offset = 0;
                        output.Add(index);
                    }
                    else
                    {
                        if (offset == 0)
                        {
                            index = i;
                        }
                        offset++;
                    }
                }
                else
                {
                    offset = 0;
                }
            }

            return output;
        }

        protected static List<string> SplitBufferIntoMessages(byte[] buffer)
        {
            return SplitBufferIntoMessages(buffer, Encoding.ASCII);
        }

        protected static List<string> SplitBufferIntoMessages(byte[] buffer, Encoding encoding)
        {
            var endOfMessageMarkers = GetMessageEndMarkers(buffer);

            var list = new List<string>(endOfMessageMarkers.Count);

            var startIndex = 0;
            foreach (var i in endOfMessageMarkers)
            {
                list.Add(encoding.GetString(buffer, startIndex, i - startIndex));
                startIndex = i + MessageEndMarker.Length;
            }

            return list;
        }

        protected virtual void RaiseDataReceived(string messages)
        {
            // if end connection flag has been set, leave w/o raising event.
            if (endConnection)
                return;

            // thread-safe way to raise event.
            var handlers = this.DataReceived;
            if (handlers == null) 
                return;

            var splitMessages = messages.Split(new [] {"\r\x1c\r"}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var h in handlers.GetInvocationList().Cast<EventHandler<DataReceivedEventArgs>>())
            {
                h.BeginInvoke(this, new DataReceivedEventArgs(splitMessages), null, null);
            }
        }

        private readonly object disconnectedLock = new object();
        
        protected virtual void RaiseDisconnected()
        {
            var currentCanRaise = Interlocked.CompareExchange(ref canRaiseDisconnected, 0, 1);
            if (currentCanRaise == 0)
                return; 

            bool lockTaken = false;
            try
            {
                lockTaken = Monitor.TryEnter(disconnectedLock);
                if (!lockTaken) 
                    return;

                var handlers = this.Disconnected;
                if (handlers != null)
                {
                    foreach (var h in handlers.GetInvocationList().Cast<EventHandler>())
                    {
                        h.BeginInvoke(this, new EventArgs(), null, null);
                    }
                }
                Interlocked.CompareExchange(ref isConnected, 0, 1);
            }
            finally
            {
                if (lockTaken) 
                    Monitor.Exit(disconnectedLock);
            }
        }

        #endregion

        #region [ Public members ]

        public void Connect(DeviceConnectionType connectionType, object endPoint)
        {
            if (connectionType != DeviceConnectionType.IPv4)
                throw new NotSupportedException("Only IPv4 connection is supported.");

            if (endPoint == null)
                throw new ArgumentNullException("endPoint");

            if (Interlocked.Read(ref isConnected) != 0) 
                throw new InvalidOperationException("Device is already connected.");

            try
            {
                var paramThreadStart = new ParameterizedThreadStart(SocketThread);
                var t = new Thread(paramThreadStart) { IsBackground = true };
                t.Start(endPoint);
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                throw;
            }
        }
        
        public void Disconnect()
        {
            endConnection = true;
            Interlocked.CompareExchange(ref isConnected, 0, 1);
        }

        public PatientParameter ParametersMask
        {
            get 
            {
                try
                {
                    rwLock.EnterReadLock();
                    return parameterMask;
                }
                finally
                {
                    rwLock.ExitReadLock();
                }
            }
            set
            {
                try
                {
                    rwLock.EnterWriteLock();
                    parameterMask = value;
                    
                }
                finally
                {
                    rwLock.ExitWriteLock();
                    parameterMaskChanged = true;
                }
            }
        }

        public int Interval
        {
            get
            {
                try
                {
                    rwLock.EnterReadLock();
                    return interval;
                }
                finally
                {
                    rwLock.ExitReadLock();
                }
            }
            set 
            {
                try
                {
                    rwLock.EnterWriteLock();
                    interval = value;
                }
                finally
                {
                    rwLock.ExitWriteLock();
                    intervalChanged = true;
                }
            }
        }

        public event EventHandler<DataReceivedEventArgs> DataReceived = (sender, args) => { };

        public event EventHandler Disconnected = (sender, args) => { };

        public bool TryConnect(DeviceConnectionType connectionType, object endPoint)
        {
            if (connectionType != DeviceConnectionType.IPv4)
                throw new NotSupportedException("Only IPv4 connection is supported.");

            if (endPoint == null)
                throw new ArgumentNullException("endPoint");

            var remoteEndPoint = endPoint as IPEndPoint;
            if (remoteEndPoint == null) 
                throw new ArgumentException("Argument of System.Net.IPEndPoint type was expected.", "endPoint");

            try
            {
                using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                {
                    //socket.ExclusiveAddressUse = true;
                    //socket.LingerState = new LingerOption(true, 0);

                    if (connectTimeout == 0)
                    {
                        socket.Connect(remoteEndPoint);
                    }
                    else
                    {
                        AsyncCallback callback = asyncResult =>
                        {
                            try
                            {
                                var s = (Socket)asyncResult.AsyncState;
                                if (asyncResult.IsCompleted && s.Connected)
                                    s.EndConnect(asyncResult);
                            }
                            catch (ObjectDisposedException)
                            {
                                // do nothing
                            }
                        };    
                        
                        IAsyncResult ar = socket.BeginConnect(remoteEndPoint, callback, socket);
                        if (!ar.AsyncWaitHandle.WaitOne(ConnectTimeout))
                        {
                            return false;
                        }
                    }

                    var buffer = new byte[socket.ReceiveBufferSize];
                    int bytesRead;
                    SocketError error;
                    do
                    {
                        bytesRead = socket.Receive(buffer, 0, buffer.Length, SocketFlags.None, out error);
                        if (bytesRead == 0 || error != SocketError.Success)
                            return false;
                    } while (bytesRead == buffer.Length && error == SocketError.Success);
                    socket.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                return false;
            }
        }

        public int ConnectTimeout 
        {
            get
            {
                return this.connectTimeout;
            }
            set 
            { 
                if (value < 0) 
                    throw new ArgumentOutOfRangeException("value");
                
                this.connectTimeout = value;
            }
        }

        public int SendReceiveTimeout
        {
            get
            {
                return this.sendReceiveTimeout;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("value");

                this.sendReceiveTimeout = value;
            }
        }

        #endregion
    }
}
