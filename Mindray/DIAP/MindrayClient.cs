﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using Gess.Device.Common;
using Gess.Device.Events;

namespace Gess.Device.Mindray.DIAP
{
    public class MindrayClient : IPatientMonitorDevice
    {

        #region [ Constructor(s) ]

        public MindrayClient(ILogger loggerInstance)
        {
            if (loggerInstance == null)
                throw new ArgumentNullException("loggerInstance");

            logger = loggerInstance;
        }

        #endregion

        #region [ Public Property(s) ]

        public PatientParameter ParametersMask
        {
            get
            {
                try
                {
                    rwLock.EnterReadLock();
                    return parameterMask;
                }
                finally
                {
                    rwLock.ExitReadLock();
                }
            }
            set
            {
                try
                {
                    rwLock.EnterWriteLock();
                    parameterMask = value;

                }
                finally
                {
                    rwLock.ExitWriteLock();
                    parameterMaskChanged = true;
                }
            }
        }

        public int Interval
        {
            get
            {
                try
                {
                    rwLock.EnterReadLock();
                    return interval;
                }
                finally
                {
                    rwLock.ExitReadLock();
                }
            }
            set
            {
                try
                {
                    rwLock.EnterWriteLock();
                    interval = value;
                }
                finally
                {
                    rwLock.ExitWriteLock();
                    intervalChanged = true;
                }
            }
        }

        public int ConnectTimeout
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int SendReceiveTimeout
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region [ Public Field(s) ]

        public event EventHandler<DataReceivedEventArgs> DataReceived;

        public event EventHandler Disconnected;

        #endregion

        #region [ Public Method(s) ]

        public void Connect(DeviceConnectionType connectionType, object endPoint)
        {

            if (connectionType != DeviceConnectionType.Serial)
            {
                throw new NotSupportedException("Only SerialPort connection is supported.");
            }

            if (endPoint == null)
            {
                throw new ArgumentNullException("endPoint");
            }

            try
            {
                ValidatePort(endPoint.ToString());

                var paramThreadStart = new ParameterizedThreadStart(DoWork);

                Thread newThread = new Thread(paramThreadStart);
                newThread.Start(endPoint.ToString());

            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                throw;
            }

        }

        public bool TryConnect(DeviceConnectionType connectionType, object endPoint)
        {
            throw new NotImplementedException();
        }

        public void Disconnect()
        {
            endConnection = true;
            Interlocked.CompareExchange(ref isConnected, 0, 1);
        }

        #endregion

        #region [ Protected Method(s) ]

        protected virtual void RaiseDisconnected()
        {
            var handlers = this.Disconnected;
            if (handlers != null)
            {
                foreach (var h in handlers.GetInvocationList().Cast<EventHandler>())
                {
                    h.BeginInvoke(this, new EventArgs(), null, null);
                }
            }
            Interlocked.CompareExchange(ref isConnected, 0, 1);
        }

        protected virtual void RaiseDataReceived(string messages)
        {
            // if end connection flag has been set, leave w/o raising event.

            // thread-safe way to raise event.
            var handlers = this.DataReceived;
            if (handlers == null)
                return;

            string[] tempMessage = new string[] { messages };


            foreach (var h in handlers.GetInvocationList().Cast<EventHandler<DataReceivedEventArgs>>())
            {
                h.BeginInvoke(this, new DataReceivedEventArgs(tempMessage), null, null);
            }
        }

        #endregion

        #region [ Private Field(s) ]

        private readonly ILogger logger;

        private volatile int interval;

        private volatile bool intervalChanged;

        private readonly ReaderWriterLockSlim rwLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        private SerialPort spMedicalDevice = null;

        private long isConnected;

        private bool endConnection = true;

        private bool parameterMaskChanged = false;

        private volatile PatientParameter parameterMask = PatientParameter.T1 | PatientParameter.T2 | PatientParameter.TD |
                                                         PatientParameter.SpO2 | PatientParameter.PR | PatientParameter.RR | PatientParameter.HR |
                                                         PatientParameter.NIBPSys | PatientParameter.NIBPDia | PatientParameter.NIBPMean;

        private volatile string tempData = string.Empty;

        #region  [ CRC Table ]

        private ushort[] crcTable = { 0x0000, 0x0108, 0x0210, 0x0318, 0x0420, 0x0528, 0x0630, 0x0738,
                                    0x0840, 0x0948, 0x0A50, 0x0B58, 0x0C60, 0x0D68, 0x0E70, 0x0F78,
                                    0x1081, 0x1189, 0x1291, 0x1399, 0x14A1, 0x15A9, 0x16B1, 0x17B9,
                                    0x18C1, 0x19C9, 0x1AD1, 0x1BD9, 0x1CE1, 0x1DE9, 0x1EF1, 0x1FF9,
                                    0x2102, 0x200A, 0x2312, 0x221A, 0x2522, 0x242A, 0x2732, 0x263A,
                                    0x2942, 0x284A, 0x2B52, 0x2A5A, 0x2D62, 0x2C6A, 0x2F72, 0x2E7A,
                                    0x3183, 0x308B, 0x3393, 0x329B, 0x35A3, 0x34AB, 0x37B3, 0x36BB,
                                    0x39C3, 0x38CB, 0x3BD3, 0x3ADB, 0x3DE3, 0x3CEB, 0x3FF3, 0x3EFB,
                                    0x4204, 0x430C, 0x4014, 0x411C, 0x4624, 0x472C, 0x4434, 0x453C,
                                    0x4A44, 0x4B4C, 0x4854, 0x495C, 0x4E64, 0x4F6C, 0x4C74, 0x4D7C,
                                    0x5285, 0x538D, 0x5095, 0x519D, 0x56A5, 0x57AD, 0x54B5, 0x55BD,
                                    0x5AC5, 0x5BCD, 0x58D5, 0x59DD, 0x5EE5, 0x5FED, 0x5CF5, 0x5DFD,
                                    0x6306, 0x620E, 0x6116, 0x601E, 0x6726, 0x662E, 0x6536, 0x643E,
                                    0x6B46, 0x6A4E, 0x6956, 0x685E, 0x6F66, 0x6E6E, 0x6D76, 0x6C7E,
                                    0x7387, 0x728F, 0x7197, 0x709F, 0x77A7, 0x76AF, 0x75B7, 0x74BF,
                                    0x7BC7, 0x7ACF, 0x79D7, 0x78DF, 0x7FE7, 0x7EEF, 0x7DF7, 0x7CFF,
                                    0x8408, 0x8500, 0x8618, 0x8710, 0x8028, 0x8120, 0x8238, 0x8330,
                                    0x8C48, 0x8D40, 0x8E58, 0x8F50, 0x8868, 0x8960, 0x8A78, 0x8B70,
                                    0x9489, 0x9581, 0x9699, 0x9791, 0x90A9, 0x91A1, 0x92B9, 0x93B1,
                                    0x9CC9, 0x9DC1, 0x9ED9, 0x9FD1, 0x98E9, 0x99E1, 0x9AF9, 0x9BF1,
                                    0xA50A, 0xA402, 0xA71A, 0xA612, 0xA12A, 0xA022, 0xA33A, 0xA232,
                                    0xAD4A, 0xAC42, 0xAF5A, 0xAE52, 0xA96A, 0xA862, 0xAB7A, 0xAA72,
                                    0xB58B, 0xB483, 0xB79B, 0xB693, 0xB1AB, 0xB0A3, 0xB3BB, 0xB2B3,
                                    0xBDCB, 0xBCC3, 0xBFDB, 0xBED3, 0xB9EB, 0xB8E3, 0xBBFB, 0xBAF3,
                                    0xC60C, 0xC704, 0xC41C, 0xC514, 0xC22C, 0xC324, 0xC03C, 0xC134,
                                    0xCE4C, 0xCF44, 0xCC5C, 0xCD54, 0xCA6C, 0xCB64, 0xC87C, 0xC974,
                                    0xD68D, 0xD785, 0xD49D, 0xD595, 0xD2AD, 0xD3A5, 0xD0BD, 0xD1B5,
                                    0xDECD, 0xDFC5, 0xDCDD, 0xDDD5, 0xDAED, 0xDBE5, 0xD8FD, 0xD9F5,
                                    0xE70E, 0xE606, 0xE51E, 0xE416, 0xE32E, 0xE226, 0xE13E, 0xE036,
                                    0xEF4E, 0xEE46, 0xED5E, 0xEC56, 0xEB6E, 0xEA66, 0xE97E, 0xE876,
                                    0xF78F, 0xF687, 0xF59F, 0xF497, 0xF3AF, 0xF2A7, 0xF1BF, 0xF0B7,
                                    0xFFCF, 0xFEC7, 0xFDDF, 0xFCD7, 0xFBEF, 0xFAE7, 0xF9FF, 0xF8F7,
                                   };

        #endregion

        #endregion

        #region [ Private Method(s) ]

        /// <summary>
        /// 
        /// </summary>
        private void DoWork(object endPoint)
        {
            if (endPoint == null)
            {
                throw new ArgumentNullException("endPoint");
            }

            string portName = endPoint.ToString();

            long result = Interlocked.CompareExchange(ref isConnected, 1, 0);

            if (result != 0)
            {
                throw new InvalidOperationException("Device is already connected.");
            }

            bool wasAborted = false;

            try
            {
                using (spMedicalDevice = new SerialPort(portName))
                {
                    spMedicalDevice.RtsEnable = true;
                    spMedicalDevice.DtrEnable = true;

                    spMedicalDevice.DataReceived += spMedicalDevice_DataReceived;
                    spMedicalDevice.Open();

                    string cmd = CommandMaker();
                    byte[] cmdByte = ByteMaker(cmd);

                    while (endConnection)
                    {
                        if (parameterMaskChanged)
                        {
                            cmd = CommandMaker();
                            cmdByte = ByteMaker(cmd);
                        }

                        spMedicalDevice.Write(cmdByte, 0, cmdByte.Length);
                        Thread.Sleep(interval);
                    }
                }
            }
            catch (ThreadAbortException)
            {
                wasAborted = true;
                Thread.ResetAbort();
            }
            catch (Exception ex)
            {
                logger.LogException(ex);
                wasAborted = true;
            }
            finally
            {
                Interlocked.CompareExchange(ref isConnected, 0, 1);
                if (wasAborted)
                {
                    RaiseDisconnected();
                }
            }

        }

        /// <summary>
        /// Validate portname
        /// </summary>
        private void ValidatePort(string portName)
        {
            if (SerialPort.GetPortNames().Where(w => w == portName.ToUpper()).Count() > 0)
            {
                throw new Exception("Port " + portName + " is not valid");
            }
            else
            {
                CheckPortAvailability(portName);
            }
        }

        /// <summary>
        /// Check port is in use or not
        /// </summary>
        private void CheckPortAvailability(string portName)
        {
            try
            {
                if (spMedicalDevice == null || !spMedicalDevice.IsOpen)
                {
                    SerialPort sp = new SerialPort(portName.ToUpper());
                    sp.Open();
                    sp.Close();
                    sp.Dispose();
                }
            }
            catch (Exception)
            {
                throw new Exception("Port " + portName + " is in use.");
            }
        }

        /// <summary>
        /// Craete DIAP command structure
        /// </summary>
        /// <returns></returns>
        private string CommandMaker()
        {
            string hexOutput = string.Empty;

            string cmdStr = "DIAP000<";

            byte[] cmdByte = null;

            int crc = 0;

            #region [ Create DIAP PARAMETER list from PatientParameter ]

            var allFilters = new[]
            {
                PatientParameter.T1, PatientParameter.T2, PatientParameter.TD,
                PatientParameter.NIBPSys, PatientParameter.NIBPDia, PatientParameter.NIBPMean,
                PatientParameter.PR, PatientParameter.HR, PatientParameter.RR,
                PatientParameter.SpO2, PatientParameter.CO, PatientParameter.CO2 
            };
            var selectedFilters = (from f in allFilters
                                   where 0 != (int)(f & parameterMask)
                                   select EnumHelper.ToDIAPParameter(f)).ToList();


            #endregion

            foreach (DIAPParameter item in selectedFilters)
            {
                cmdStr += item.ToString() + ";";
            }

            cmdStr = cmdStr.Substring(0, cmdStr.Length - 1);
            cmdStr += ">";

            hexOutput = ConvertStringToHex(cmdStr);

            cmdByte = ByteMaker(hexOutput);

            crc = CrcMaker(0, cmdByte.Length, cmdByte);

            cmdStr += String.Format("{0:X}", crc);

            hexOutput = ConvertStringToHex(cmdStr) + "0A";

            return hexOutput;

        }

        /// <summary>
        /// Manages converting ascii string commandto Hex format
        /// </summary>
        /// <param name="str">string</param>
        /// <returns>string of hex code</returns>
        private string ConvertStringToHex(string str)
        {
            string hexOutput = "";
            char[] values = str.ToCharArray();

            foreach (char letter in values)
            {
                // Get the integral value of the character. 
                int value = Convert.ToInt32(letter);
                // Convert the decimal value to a hexadecimal value in string form. 
                hexOutput += String.Format("{0:X}", value);
            }

            return hexOutput;
        }

        /// <summary>
        /// Convert string command to array of byte
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private byte[] ByteMaker(string cmd)
        {

            return Enumerable.Range(0, cmd.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(cmd.Substring(x, 2), 16))
                .ToArray();

        }

        /// <summary>
        /// Create CRC code for sending to device.
        /// </summary>
        /// <param name="lastCrc"></param>
        /// <param name="sizeOfBuffer"></param>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private int CrcMaker(int lastCrc, int sizeOfBuffer, byte[] buffer)
        {
            byte index;

            for (int i = 0; i < sizeOfBuffer; i++)
            {
                byte temp = ((byte)(lastCrc & 0x00FF));

                index = (byte)((byte)buffer.GetValue(i) ^ temp);

                lastCrc = ((lastCrc >> 8) & 0x00FF) ^ (crcTable[index]);
            }

            return (lastCrc);
        }

        /// <summary>
        /// Manages rescieving date from serial port
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spMedicalDevice_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

            tempData += sp.ReadExisting();

            if (tempData.Contains('>'))
            {
                //_SampleData.Add(tempData);
                RaiseDataReceived(tempData);
                tempData = string.Empty;
            }
        }

        #endregion

    }
}
