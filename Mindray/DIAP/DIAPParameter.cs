﻿using Gess.Device.Common;

namespace Gess.Device.Mindray.DIAP
{
    // ReSharper disable InconsistentNaming

    public enum DIAPParameter
    {
        [PatientParameterValue(PatientParameter.T1)]
        t1 = 200,

        [PatientParameterValue(PatientParameter.T2)]
        t2 = 201,

        [PatientParameterValue(PatientParameter.TD)]
        deltaT = 202,

        nibp = 170,

        [PatientParameterValue(PatientParameter.SpO2)]
        spo2 = 160,

        [PatientParameterValue(PatientParameter.PR)]
        spo2HR = 161,

        [PatientParameterValue(PatientParameter.HR)]
        hr = 101,

        [PatientParameterValue(PatientParameter.RR)]
        resp = 151,

        [PatientParameterValue(PatientParameter.CO)]
        co = 123,

        [PatientParameterValue(PatientParameter.CO2)]
        co2 = 124
    }

    //ReSharper restore InconsistentNaming
}
