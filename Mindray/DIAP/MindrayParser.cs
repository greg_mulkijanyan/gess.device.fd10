﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gess.Device.Common;
using Gess.Device.Events;
using Gess.Device.Messages;

namespace Gess.Device.Mindray.DIAP
{
    public class MindrayParser : IPatientDataSource
    {

        #region [ Constructor(s) ]

        /// <summary>
        /// Cosntructor.
        /// </summary>
        /// <param name="monitorDevice">IPatientMonitorDevice</param>
        /// <param name="loggerInstance">ILogger</param>
        public MindrayParser(IPatientMonitorDevice monitorDevice, ILogger loggerInstance)
        {
            if (monitorDevice == null)
            {
                throw new ArgumentNullException("monitorDevice");
            }

            if (loggerInstance == null)
            {
                throw new ArgumentNullException("loggerInstance");
            }


            MonitorDevice = monitorDevice;
            MonitorDevice.DataReceived += OnDataReceived;

            logger = loggerInstance;

        }

        #endregion

        #region [ Public Field(s) ]

        public event EventHandler<PatientStateData> PatientDataReceived;

        public event EventHandler<TechnicalAlarmEventArgs> TechnicalAlarm;

        public event EventHandler<PhysiologicalAlarmEventArgs> PhysiologicalAlarm;

        public event EventHandler<AlarmLimitChangedEventArgs> ParameterAlarmLimitChanged;

        public event EventHandler<AlarmLevelChangedEventArgs> AlarmLevelChanged;

        #endregion

        #region [ Public Method(s) ]

        public double GetT1()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.t1).SingleOrDefault();
                    if (temp != null)
                    {
                        if (!double.TryParse(temp.Value, out result))
                        {
                            result = double.NaN;
                        }
                        else
                        {
                            result = result / 10;
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double GetT2()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.t2).SingleOrDefault();
                    if (temp != null)
                    {
                        if (!double.TryParse(temp.Value, out result))
                        {
                            result = double.NaN;
                        }
                        else
                        {
                            result = result / 10;
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double GetTD()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.deltaT).SingleOrDefault();
                    if (temp != null)
                    {
                        if (!double.TryParse(temp.Value, out result))
                        {
                            result = double.NaN;
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double Get_ECG_NIBPSis()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.nibp).SingleOrDefault();
                    if (temp != null)
                    {
                        string tempStr = temp.Value;
                        if (!string.IsNullOrWhiteSpace(tempStr))
                        {

                            if (!double.TryParse(tempStr.Split(separatorChar).GetValue(0).ToString(), out result))
                            {
                                result = double.NaN;
                            }
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double Get_ECG_NIBPDia()
        {
            double result = double.NaN;

            try
            {

                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.nibp).SingleOrDefault();
                    if (temp != null)
                    {
                        string tempStr = temp.Value;
                        if (!string.IsNullOrWhiteSpace(tempStr))
                        {
                            if (!double.TryParse(tempStr.Split(separatorChar).GetValue(1).ToString(), out result))
                            {
                                result = double.NaN;
                            }
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double Get_ECG_NIBPMean()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.nibp).SingleOrDefault();
                    if (temp != null)
                    {
                        string tempStr = temp.Value;
                        if (!string.IsNullOrWhiteSpace(tempStr))
                        {
                            if (!double.TryParse(tempStr.Split(separatorChar).GetValue(2).ToString(), out result))
                            {
                                result = double.NaN;
                            }
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double Get_SpO2_PR()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.spo2HR).SingleOrDefault();
                    if (temp != null)
                    {
                        if (!double.TryParse(temp.Value, out result))
                        {
                            result = double.NaN;
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double Get_SpO2()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.spo2).SingleOrDefault();
                    if (temp != null)
                    {
                        if (!double.TryParse(temp.Value, out result))
                        {
                            result = double.NaN;
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double Get_ECG_RR()
        {
            double result = double.NaN;

            try
            {
                if (receivedDIAPValues != null)
                {
                    DIAPValue temp = receivedDIAPValues.Where(w => w.Key == DIAPParameter.resp).SingleOrDefault();
                    if (temp != null)
                    {
                        if (!double.TryParse(temp.Value, out result))
                        {
                            result = double.NaN;
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        public double Get_CO()
        {
            throw new NotImplementedException();
        }

        public double Get_CO2()
        {
            throw new NotImplementedException();
        }

        public bool Supports(PatientParameter patientParameter)
        {
            throw new NotImplementedException();
        }

        public Dictionary<PatientParameter, string> GetMeasurementUnits()
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(PatientParameter parameter, out double value)
        {
            throw new NotImplementedException();
        }

        public double GetUpperLimit(PatientParameter value)
        {
            throw new NotImplementedException();
        }

        public double GetLowerLimit(PatientParameter value)
        {
            throw new NotImplementedException();
        }

        public IPatientMonitorDevice MonitorDevice
        {
            get;
            protected set;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        public Guid UniqueId
        {
            get { throw new NotImplementedException(); }
        }

        public bool Subscribe(out long subscriptionId)
        {
            throw new NotImplementedException();
        }

        public bool IsSubscribed
        {
            get { throw new NotImplementedException(); }
        }

        public bool TryDequeue(long subscriptionId, out AbstractPatientMessage message)
        {
            throw new NotImplementedException();
        }

        public void Unsubscribe(long subscriptionId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region [ Protected Method(s) ]

        protected virtual void OnDataReceived(object sender, DataReceivedEventArgs args)
        {
            if (sender == null)
            {
                throw new ArgumentNullException("sender");
            }

            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            string[] tempItems = null;
            string tempValue = string.Empty;

            string tempRecievedMessage = args.Data[0];

            string[] values = tempRecievedMessage.Substring(tempRecievedMessage.IndexOf('<') + 1, tempRecievedMessage.IndexOf('>') - tempRecievedMessage.IndexOf('<') - 1).Split(';');

            foreach (string item in values)
            {
                tempItems = item.Split('=');
                tempValue = tempItems.GetValue(1).ToString();

                DIAPParameter tempKey = ((DIAPParameter[])Enum.GetValues(typeof(DIAPParameter))).Where(w => w.ToString().ToLower() == tempItems.GetValue(0).ToString()).SingleOrDefault();

                receivedDIAPValues.Add(new DIAPValue(tempKey, tempValue));
            }

            var patientData = new PatientStateData
            {
                T1 = GetT1(),
                T2 = GetT2(),
                TD = GetTD(),
                NIBPDia = Get_ECG_NIBPDia(),
                NIBPSys = Get_ECG_NIBPSis(),
                NIBPMean = Get_ECG_NIBPMean(),
                SpO2 = Get_SpO2(),
                PR = Get_SpO2_PR(),
                RR = Get_ECG_RR(),
                //HR = get
                //FirstName = GetPatientFirstName(),
                //LastName = GetPatientLastName()
            };

            RaisePatientDataReceived(patientData);

        }

        #endregion

        #region [ Private Field(s) ]

        private readonly ILogger logger;

        private List<DIAPValue> receivedDIAPValues = null;

        private const char separatorChar = ',';

        #endregion

        #region [ Private Method(s) ]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="patientStateData"></param>
        private void RaisePatientDataReceived(PatientStateData patientStateData)
        {
            var handlers = this.PatientDataReceived;
            if (handlers != null)
            {
                handlers(this, patientStateData);
            }
        }

        #endregion

    }


    public class DIAPValue
    {

        #region [ Cosntructor(s) ]

        public DIAPValue(DIAPParameter key, string value)
        {
            this.key = key;
            this.value = value;
        }

        #endregion

        #region [ Public Property(s) ]

        public DIAPParameter Key
        {
            get
            {
                return key;
            }
        }

        public string Value
        {
            get
            {
                return value;
            }
        }

        #endregion

        #region [ Private Field(s) ]

        DIAPParameter key;

        string value = string.Empty;

        #endregion
    }

}
