﻿namespace Gess.Device.Messages
{
    /// <summary>
    /// Specifies what kind of message was received from patient monitoring device.
    /// </summary>
    public enum PatientMessageType
    {
        /// <summary>
        /// Patient health parameters, like temperature, blood-pressure, etc.
        /// </summary>
        PatientParameters,

        /// <summary>
        /// Physiological alarm, when one or more of patient's parameters are out of allowed range.
        /// </summary>
        PhysiologicalAlarm,

        /// <summary>
        /// Technical alarm, when one or more of hardware components report error.
        /// </summary>
        TechnicalAlarm,

        /// <summary>
        /// Change of patient's parameters allowed range.
        /// </summary>
        AlarmLimitsChange,

        /// <summary>
        /// Change of one or more of patient monitoring device's configuration parameters.
        /// </summary>
        ConfigurationParameterChange
    }
}
