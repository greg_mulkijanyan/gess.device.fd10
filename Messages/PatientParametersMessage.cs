﻿using System;
using Gess.Device.Common;

namespace Gess.Device.Messages
{
    public class PatientParametersMessage : AbstractPatientMessage
    {
        public PatientParametersMessage(PatientStateData patientData)
            : base(PatientMessageType.PatientParameters)
        {
            if (patientData == null)
                throw new ArgumentNullException("patientData");

            this.PatientData = patientData;
        }

        public PatientStateData PatientData { get; protected set; }
    }
}
