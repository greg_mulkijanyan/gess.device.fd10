﻿using System;
using Gess.Device.Events;

namespace Gess.Device.Messages
{
    public class TechnicalAlarmMessage : AbstractPatientMessage
    {
        public TechnicalAlarmMessage(TechnicalAlarmEventArgs technicalAlarm)
            : base(PatientMessageType.TechnicalAlarm)
        {
            if (technicalAlarm == null)
                throw new ArgumentNullException("technicalAlarm");

            this.TechnicalAlarm = technicalAlarm;
        }

        public TechnicalAlarmEventArgs TechnicalAlarm { get; protected set; }
    }
}
