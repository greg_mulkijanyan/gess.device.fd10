﻿using System;

namespace Gess.Device.Messages
{
    public abstract class AbstractPatientMessage
    {
        protected AbstractPatientMessage(PatientMessageType messageType)
        {
            this.MessageType = messageType;

            this.MessageTimestamp = DateTime.Now;
        }

        public PatientMessageType MessageType { get; protected set; }

        public DateTime MessageTimestamp { get; protected set; }
    }
}
