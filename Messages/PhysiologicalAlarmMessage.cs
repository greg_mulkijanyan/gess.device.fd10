﻿using System;
using Gess.Device.Events;

namespace Gess.Device.Messages
{
    public class PhysiologicalAlarmMessage : AbstractPatientMessage
    {
        public PhysiologicalAlarmMessage(PhysiologicalAlarmEventArgs physiologicalAlarm)
            : base(PatientMessageType.PhysiologicalAlarm)
        {
            if (physiologicalAlarm == null)
                throw new ArgumentNullException("physiologicalAlarm");

            PhysiologicalAlarm = physiologicalAlarm;
        }

        public PhysiologicalAlarmEventArgs PhysiologicalAlarm { get; protected set; }
    }
}
