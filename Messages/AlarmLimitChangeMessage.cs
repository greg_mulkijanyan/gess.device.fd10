﻿using System;
using System.Collections.Generic;
using Gess.Device.Common;

namespace Gess.Device.Messages
{
    public class AlarmLimitChangeMessage : AbstractPatientMessage
    {
        public AlarmLimitChangeMessage(IEnumerable<AlarmLimitChangeInfo> alarmInfo)
            : base(PatientMessageType.AlarmLimitsChange)
        {
            if (alarmInfo == null) 
                throw new ArgumentNullException("alarmInfo");
            
            AlarmLimitChange = new List<AlarmLimitChangeInfo>(alarmInfo);
        }

        public AlarmLimitChangeMessage(params AlarmLimitChangeInfo[] alarmInfos)
            : base(PatientMessageType.AlarmLimitsChange)
        {
            if (alarmInfos == null)
                throw new ArgumentNullException("alarmInfos");

            AlarmLimitChange = new List<AlarmLimitChangeInfo>(alarmInfos);
        }

        public List<AlarmLimitChangeInfo> AlarmLimitChange { get; protected set; }
    }
}
