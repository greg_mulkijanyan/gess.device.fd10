﻿using System;
using System.Diagnostics;

namespace Gess.Device
{
    public interface ILogger
    {
        void LogLocally(string messageDetails);

        void LogRemotely(string messageDetails);

        void LogException(Exception ex);

        bool IsRemoteLoggingAvailable();
    }

    public class DummyLogger : ILogger
    {
        public void LogLocally(string messageDetails)
        {
            Console.WriteLine(messageDetails);
            Debug.Print(messageDetails);
        }

        public void LogRemotely(string messageDetails)
        {
            Console.WriteLine(messageDetails);
            Debug.Print(messageDetails);
        }

        public void LogException(Exception ex)
        {
            Console.WriteLine(ex.Message + ex.StackTrace);
            Debug.Print(ex.Message + ex.StackTrace);
        }

        public bool IsRemoteLoggingAvailable()
        {
            return true;
        }
    }
}
