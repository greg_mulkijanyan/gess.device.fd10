﻿namespace Gess.Device.Common
{
    public class AlarmLevelChangeInfo
    {
        public PatientParameter Parameter { get; set; }

        public int AlarmLevel { get; set; }
    }
}
