﻿using System;
using Gess.Device.Febroskript;
using Gess.Device.Mindray.HL7;

namespace Gess.Device.Common
{
    public static class DeviceConnectorFactory
    {
        public static IPatientMonitorDevice Create(string model, DeviceConnectionType connectionType, object endPoint)
        {
            if (model == null) 
                throw new ArgumentNullException("model");

            if (endPoint == null) 
                throw new ArgumentNullException("endPoint");

            switch (model.ToLowerInvariant())
            {
                case "mindray" :
                    return connectionType == DeviceConnectionType.IPv4 ? new MindrayIpClient(new DummyLogger()) : null;

                case "febroskript" :
                    return new FebroskriptDevice(new DummyLogger());

                default :
                    throw new NotSupportedException("Specified PMD model is not supported.");
            }
        }
    }
}
