﻿using System.Collections.Generic;
using System.Linq;
using Gess.Device.Mindray.DIAP;
using Gess.Device.Mindray.HL7;

namespace Gess.Device.Common
{
    // ReSharper disable InconsistentNaming
    /// <summary>
    /// Helper class, which converts from HL7 patient parameter enum to our parameter enum.
    /// </summary>
    public static class EnumHelper
    {

        private static readonly Dictionary<PatientParameter, HL7Parameter> PatientToHl7 = new Dictionary<PatientParameter, HL7Parameter>();

        private static readonly Dictionary<PatientParameter, DIAPParameter> PatientToDIAP = new Dictionary<PatientParameter, DIAPParameter>();


        static EnumHelper()
        {
            PatientToHl7[PatientParameter.T1] = HL7Parameter.T1;
            PatientToHl7[PatientParameter.T2] = HL7Parameter.T2;
            PatientToHl7[PatientParameter.TD] = HL7Parameter.TD;
            PatientToHl7[PatientParameter.NIBPDia] = HL7Parameter.NIBPDia;
            PatientToHl7[PatientParameter.NIBPSys] = HL7Parameter.NIBPSys;
            PatientToHl7[PatientParameter.NIBPMean] = HL7Parameter.NIBPMean;
            PatientToHl7[PatientParameter.CO] = HL7Parameter.CO;
            PatientToHl7[PatientParameter.CO2] = HL7Parameter.CO2;
            PatientToHl7[PatientParameter.PR] = HL7Parameter.PR;
            PatientToHl7[PatientParameter.RR] = HL7Parameter.RR;
            PatientToHl7[PatientParameter.HR] = HL7Parameter.HR;
            PatientToHl7[PatientParameter.SpO2] = HL7Parameter.SpO2;


            PatientToDIAP[PatientParameter.T1] = DIAPParameter.t1;
            PatientToDIAP[PatientParameter.T2] = DIAPParameter.t2;
            PatientToDIAP[PatientParameter.TD] = DIAPParameter.deltaT;
            PatientToDIAP[PatientParameter.CO] = DIAPParameter.co;
            PatientToDIAP[PatientParameter.CO2] = DIAPParameter.co2;
            PatientToDIAP[PatientParameter.PR] = DIAPParameter.spo2HR;
            PatientToDIAP[PatientParameter.RR] = DIAPParameter.resp;
            PatientToDIAP[PatientParameter.HR] = DIAPParameter.hr;
            PatientToDIAP[PatientParameter.SpO2] = DIAPParameter.spo2;
            PatientToDIAP[PatientParameter.NIBPDia] = DIAPParameter.nibp;
            PatientToDIAP[PatientParameter.NIBPMean] = DIAPParameter.nibp;
            PatientToDIAP[PatientParameter.NIBPSys] = DIAPParameter.nibp;

        }



        public static PatientParameter ToPatientParameter(HL7Parameter hl7Param)
        {
            return (from kvp in PatientToHl7
                    where kvp.Value == hl7Param
                    select kvp.Key).FirstOrDefault();
        }

        public static HL7Parameter ToHL7Parameter(PatientParameter patientParam)
        {
            return PatientToHl7[patientParam];
        }



        public static PatientParameter ToPatientParameter(DIAPParameter diapParam)
        {
            return (from kvp in PatientToDIAP
                    where kvp.Value == diapParam
                    select kvp.Key).FirstOrDefault();
        }

        public static DIAPParameter ToDIAPParameter(PatientParameter patientParam)
        {
            return PatientToDIAP[patientParam];
        }


    }
    // ReSharper restore InconsistentNaming
}
