﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Gess.Device.Common
{
    // ReSharper disable InconsistentNaming
    /// <summary>
    /// Aggregated patient health parameters.
    /// </summary>
    public class PatientStateData
    {
        /// <summary>
        /// Non-specific temperature 1.
        /// </summary>
        public double? T1 { get; set; }

        /// <summary>
        /// Non-specific temperature 2.
        /// </summary>
        public double? T2 { get; set; }

        /// <summary>
        /// Temperature delta.
        /// </summary>
        public double? TD { get; set; }

        /// <summary>
        /// Non-invasive blood pressure systolic.
        /// </summary>
        public double? NIBPSys { get; set; }

        /// <summary>
        /// Non-invasive blood pressure diastolic.
        /// </summary>
        public double? NIBPDia { get; set; }

        /// <summary>
        /// Non-invasive blood pressure mean.
        /// </summary>
        public double? NIBPMean { get; set; }

        /// <summary>
        /// Heart rate.
        /// </summary>
        public double? HR { get; set; }

        /// <summary>
        /// Oxigen saturation.
        /// </summary>
        public double? SpO2 { get; set; }

        /// <summary>
        /// Respiration rate.
        /// </summary>
        public double? RR { get; set; }

        /// <summary>
        /// Pulse rate.
        /// </summary>
        public double? PR { get; set; }

        /// <summary>
        /// Gets value of property for specified patient parameter.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public double? GetValue(PatientParameter parameter)
        {
            switch (parameter)
            {
                case PatientParameter.T1:
                    return this.T1;

                case PatientParameter.T2:
                    return this.T2;

                case PatientParameter.TD:
                    return this.TD;

                case PatientParameter.SpO2:
                    return this.SpO2;

                case PatientParameter.PR:
                    return this.PR;

                case PatientParameter.RR:
                    return this.RR;

                case PatientParameter.NIBPDia:
                    return this.NIBPDia;

                case PatientParameter.NIBPSys:
                    return this.NIBPSys;

                case PatientParameter.NIBPMean:
                    return this.NIBPMean;

                case PatientParameter.HR:
                    return this.HR;

                default:
                    // throw new InvalidEnumArgumentException("parameter", (int) parameter, typeof(PatientParameter));
                    return null;
            }
        }

        /// <summary>
        /// Sets property value for specified patient parameter.
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="value"></param>
        public void SetValue(PatientParameter parameter, double? value)
        {
            switch (parameter)
            {
                case PatientParameter.T1:
                    this.T1 = value;
                    break;

                case PatientParameter.T2:
                    this.T2 = value;
                    break;

                case PatientParameter.TD:
                    this.TD = value;
                    break;

                case PatientParameter.SpO2:
                    this.SpO2 = value;
                    break;

                case PatientParameter.PR:
                    this.PR = value;
                    break;

                case PatientParameter.RR:
                    this.RR = value;
                    break;

                case PatientParameter.NIBPDia:
                    this.NIBPDia = value;
                    break;

                case PatientParameter.NIBPSys:
                    this.NIBPSys = value;
                    break;

                case PatientParameter.NIBPMean:
                    this.NIBPMean = value;
                    break;

                case PatientParameter.HR: 
                    this.HR = value;
                    break;

                default:
                    throw new InvalidEnumArgumentException("parameter", (int) parameter, typeof (PatientParameter));
            }
        }

        /// <summary>
        /// Indexer property
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public double? this[PatientParameter parameter]
        {
            get
            {
                return GetValue(parameter);
            }
            set
            {
                SetValue(parameter, value);
            }
        }

        private static readonly PatientParameter[] enumMemberValues = (from object p in Enum.GetValues(typeof (PatientParameter))
                                                                       let param = (PatientParameter) p
                                                                       select param).ToArray();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="excludedValues"></param>
        /// <returns></returns>
        public bool HasAnyValidValue(params double[] excludedValues)
        {
            var values = from p in enumMemberValues 
                         select this[p];

            foreach (var v in values)
            {
                if (v == null || double.IsNaN(v.Value)) 
                    continue;

                if (excludedValues == null || excludedValues.Length == 0) 
                    continue;

                if (excludedValues.Contains(v.Value)) 
                    continue;

                return true;
            }

            return false;
        }

    }
    // ReSharper restore InconsistentNaming
}
