﻿using System;
using System.Collections.Concurrent;
using System.Reflection;

namespace Gess.Device.Common
{
    internal static class EnumMappingHelper
    {
        private static readonly ConcurrentDictionary<string, ConcurrentDictionary<object, object>> EnumMembersCache;

        static EnumMappingHelper()
        {
            EnumMembersCache = new ConcurrentDictionary<string, ConcurrentDictionary<object, object>>();
        }

        // ReSharper disable InconsistentNaming
        internal static PatientParameter GetPatientParameterValue<T>(this T patientParameter)
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("Argument of enumeration type was expected.", "patientParameter");
            }

            ConcurrentDictionary<object, object> entries;
            if (!EnumMembersCache.TryGetValue(type.FullName, out entries))
            {
                EnumMembersCache.TryAdd(type.FullName, entries = new ConcurrentDictionary<object, object>());
            }

            object result;
            if (entries.TryGetValue(patientParameter, out result))
            {
                return (PatientParameter) result;
            }

            var memberInfo = type.GetMember(patientParameter.ToString());
            if (memberInfo.Length == 0)
            {
                throw new ArgumentException("patientParameter");
            }

            var enumField = memberInfo[0] as FieldInfo;
            if (enumField == null)
                throw new ArgumentException("patientParameter");

            var attributeList = enumField.GetCustomAttributes(typeof(PatientParameterValueAttribute), false);

            if (attributeList.Length == 0)
                return 0;

            var output = ((PatientParameterValueAttribute) attributeList[0]).MappedValue;
            entries.TryAdd(patientParameter, output);
            return output;
        }
        // ReSharper restore InconsistentNaming
    }
}
