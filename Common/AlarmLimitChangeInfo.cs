﻿namespace Gess.Device.Common
{
    public class AlarmLimitChangeInfo
    {
        public PatientParameter Parameter { get; set; }

        public ParameterLimitType Type { get; set; }

        public double Value { get; set; }

        public double OldValue { get; set; }
    }
}
