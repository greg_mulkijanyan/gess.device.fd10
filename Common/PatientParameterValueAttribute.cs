﻿using System;

namespace Gess.Device.Common
{
    // ReSharper disable InconsistentNaming
    [AttributeUsage(AttributeTargets.Field)]
    public class PatientParameterValueAttribute : Attribute
    {
        public PatientParameterValueAttribute(PatientParameter value)
        {
            MappedValue = value;
        }

        public PatientParameter MappedValue { get; set; }
    }
    // ReSharper restore InconsistentNaming
}
