﻿using System;

namespace Gess.Device.Common
{
    // ReSharper disable InconsistentNaming
    [Flags]
    public enum PatientParameter
    {
        /// <summary>
        /// Unknown or unlisted parameter.
        /// </summary>
        Unknown = 0x0,

        /// <summary>
        /// Non-specific temperature 1.
        /// </summary>
        T1 = 0x1,

        /// <summary>
        /// Non-specific temperature 2.
        /// </summary>
        T2 = 0x2,

        /// <summary>
        /// Non-specific temperature delta.
        /// </summary>
        TD = 0x4,

        /// <summary>
        /// Non-invasive blood pressure systolic.
        /// </summary>
        NIBPSys = 0x8,

        /// <summary>
        /// Non-invasive blood pressure diastolic.
        /// </summary>
        NIBPDia = 0x10,

        /// <summary>
        /// Non-invasive blood-pressure mean.
        /// </summary>
        NIBPMean = 0x20,

        /// <summary>
        /// Respiration rate.
        /// </summary>
        RR = 0x40,

        /// <summary>
        /// Heart rate.
        /// </summary>
        HR = 0x80,
        
        /// <summary>
        /// Blood saturation with O2 (oxygen).
        /// </summary>
        SpO2 = 0x100,

        /// <summary>
        /// Pulse rate via SpO2 module.
        /// </summary>
        PR = 0x200,

        /// <summary>
        /// CO2 (carbon dioxide) concentration.
        /// </summary>
        CO2 = 0x400,

        /// <summary>
        /// Cardiac output.
        /// </summary>
        CO = 0x800
    }

    // ReSharper restore InconsistentNaming
}
