﻿namespace Gess.Device.Common
{
    // ReSharper disable InconsistentNaming
    public enum DeviceConnectionType
    {
        IPv4,

        Serial
    }
    // ReSharper restore InconsistentNaming
}
