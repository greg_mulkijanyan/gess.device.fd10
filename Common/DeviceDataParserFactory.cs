﻿using System;
using Gess.Device.Febroskript;
using Gess.Device.Mindray.HL7;

namespace Gess.Device.Common
{
    public static class DeviceDataParserFactory
    {
        private const string Mindray = "mindray";

        private const string Febroskript = "febroskript";

        public static IPatientDataSource Create(string model, DeviceConnectionType connectionType, object endPoint)
        {
            if (model == null) 
                throw new ArgumentNullException("model");

            if (endPoint == null) 
                throw new ArgumentNullException("endPoint");

            IPatientMonitorDevice connector = DeviceConnectorFactory.Create(model, connectionType, endPoint);
            if (connector == null)
                throw new NotImplementedException("Current model is not supported.");

            var lowerModel = model.ToLowerInvariant();
            
            switch (lowerModel)
            {
                case Mindray :
                    return connectionType == DeviceConnectionType.IPv4 ? new MindrayHL7Parser(connector, new DummyLogger()) : null;

                case Febroskript:
                    return new FebroskriptDataSource(connector, new DummyLogger());

                default :
                    throw new NotSupportedException("The specified PMD model is not supported.");
            }
        }
    }
}
