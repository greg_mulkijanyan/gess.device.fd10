﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Gess.Device.Common;
using Gess.Device.Events;
using Gess.Device.Messages;

namespace Gess.Device.Febroskript
{
    public class FebroskriptDataSource : IPatientDataSource
    {
        #region [ Constructor(s) ]

        public FebroskriptDataSource(IPatientMonitorDevice monitorDevice, ILogger logger)
        {
            if (monitorDevice == null) 
                throw new ArgumentNullException("monitorDevice");

            if (logger == null) 
                throw new ArgumentNullException("logger");

            loggerInstance = logger;
            MonitorDevice = monitorDevice;
            MonitorDevice.DataReceived += OnDataReceived;

            currentPatientValues = new ConcurrentDictionary<PatientParameter, double?>();
            currentPatientValues[PatientParameter.T1] = null;
            currentPatientValues[PatientParameter.T2] = null;
            currentPatientValues[PatientParameter.PR] = null;

            UniqueId = new Guid();
        }

        #endregion

        #region [ Private members ]

        private readonly ILogger loggerInstance;

        private readonly ConcurrentDictionary<PatientParameter, double?> currentPatientValues;

        #endregion

        #region [ Protected members ]

        protected virtual void OnDataReceived(object sender, DataReceivedEventArgs args)
        {
            if (sender == null) 
                throw new ArgumentNullException("sender");

            if (sender == null) 
                throw new ArgumentNullException("args");

            foreach (var s in args.Data)
            {
                try
                {
                    var cleanedData = s.Replace("HECKEL0<", string.Empty);
                    cleanedData = cleanedData.Replace(">", string.Empty);
                    
                    var parts = cleanedData.Split(new [] {';'}, StringSplitOptions.RemoveEmptyEntries);
                    var patientState = new PatientStateData();
                    bool hasPatientData = false;

                    var linq = from x in parts
                               where String.Compare(x, "heckel0<", StringComparison.InvariantCultureIgnoreCase) != 0 &&
                                     String.Compare(x, ">", StringComparison.InvariantCultureIgnoreCase) != 0 
                               select x;

                    foreach (string p in linq)
                    {
                        var valueString = p.Substring(p.IndexOf("=", StringComparison.InvariantCulture) + 1).Trim();
                        var subParts = p.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        double parsedValue;
                        if (!Double.TryParse(valueString, out parsedValue))
                        {
                            parsedValue = Double.NaN;
                        }
                        
                        PatientParameter parameter;

                        switch (subParts[0])
                        {
                            case "T1" :
                                patientState.T1 = parsedValue;
                                parameter = PatientParameter.T1;
                                hasPatientData = true;
                                break;
                            
                            case "T2" :
                                patientState.T2 = parsedValue;
                                parameter = PatientParameter.T2;
                                hasPatientData = true;
                                break;
                            
                            case "HR":
                                patientState.PR = parsedValue;
                                parameter = PatientParameter.PR;
                                hasPatientData = true;
                                break;
                            
                            default:
                                continue;
                        }

                        // subPart[1] will always be '='

                        switch (subParts[2])
                        {
                            case "<" :
                            case ">" :

                                var alarmArgs = new PhysiologicalAlarmEventArgs
                                                    {
                                                        Message = parameter.ToString(),
                                                        AlarmCode = "0",
                                                        AlarmLevel = 1
                                                    };
                                RaisePhysiologicalAlarm(alarmArgs);
                                break;
                        }
                    }

                    if (hasPatientData)
                        RaisePatientDataReceived(patientState);
                }
                catch (Exception ex)
                {
                    loggerInstance.LogException(ex);
                }
            }
        }

        protected virtual void RaiseTechnicalAlarm(TechnicalAlarmEventArgs args)
        {
            if (args == null)
                throw new ArgumentNullException("args");

            RaiseEventAsync(this.PhysiologicalAlarm, args);
        }

        protected virtual void RaisePhysiologicalAlarm(PhysiologicalAlarmEventArgs args)
        {
            if (args == null) 
                throw new ArgumentNullException("args");
            
            RaiseEventAsync(this.PhysiologicalAlarm, args);
        }

        protected virtual void RaisePatientDataReceived(PatientStateData patientStateData)
        {
            if (patientStateData == null) 
                throw new ArgumentNullException("patientStateData");

            RaiseEventAsync(this.PatientDataReceived, patientStateData);
        }

        /// <summary>
        /// Utility method to safely raise events.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="classEvent"></param>
        /// <param name="eventArgs"></param>
        private void RaiseEventAsync<T>(MulticastDelegate classEvent, T eventArgs)
        {
            var handlers = classEvent;
            if (handlers == null) 
                return;

            if (classEvent.GetType() != typeof(EventHandler<T>))
            {
                throw new ArgumentException("Invalid type parameter for this event.", "eventArgs");
            }

            var invocationList = classEvent.GetInvocationList().Cast<EventHandler<T>>();
            foreach (var h in invocationList)
            {
                h.BeginInvoke(this, eventArgs, null, null);
            }
        }

        #endregion

        #region [ Public members ]
        
        public double GetT1()
        {
            double output;
            return TryGetValue(PatientParameter.PR, out output) ? output : Double.NaN;
        }

        public double GetT2()
        {
            double output;
            return TryGetValue(PatientParameter.PR, out output) ? output : Double.NaN;
        }

        public double GetTD()
        {
            throw new NotImplementedException();
        }

        public double Get_ECG_NIBPSis()
        {
            throw new NotImplementedException();
        }

        public double Get_ECG_NIBPDia()
        {
            throw new NotImplementedException();
        }

        public double Get_ECG_NIBPMean()
        {
            throw new NotImplementedException();
        }

        public double Get_SpO2_PR()
        {
            double output;
            return TryGetValue(PatientParameter.PR, out output) ? output : Double.NaN;
        }

        public double Get_SpO2()
        {
            throw new NotImplementedException();
        }

        public double Get_ECG_RR()
        {
            throw new NotImplementedException();
        }

        public double Get_CO()
        {
            throw new NotImplementedException();
        }

        public double Get_CO2()
        {
            throw new NotImplementedException();
        }

        public bool Supports(PatientParameter patientParameter)
        {
            return patientParameter == PatientParameter.T1 || 
                   patientParameter == PatientParameter.T2 ||
                   patientParameter == PatientParameter.PR;
        }

        public Dictionary<PatientParameter, string> GetMeasurementUnits()
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(PatientParameter parameter, out double value)
        {
            double? storedValue;
            bool result = currentPatientValues.TryGetValue(parameter, out storedValue);
            if (result && storedValue.HasValue && !Double.IsNaN(storedValue.Value))
            {
                value = storedValue.Value;
                return true;
            }
            value = Double.NaN;
            return false;
        }

        public double GetUpperLimit(PatientParameter value)
        {
            throw new NotImplementedException();
        }

        public double GetLowerLimit(PatientParameter value)
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            var count = this.currentPatientValues.Count;
            var keys = new PatientParameter[count];
            currentPatientValues.Keys.CopyTo(keys, count);

            foreach (var k in keys)
            {
                double? d;
                currentPatientValues.TryRemove(k, out d);
            }
        }

        public event EventHandler<PatientStateData> PatientDataReceived = (s, a) => { };

        public event EventHandler<TechnicalAlarmEventArgs> TechnicalAlarm = (s, a) => { };

        public event EventHandler<PhysiologicalAlarmEventArgs> PhysiologicalAlarm = (s, a) => { };

        public event EventHandler<AlarmLimitChangedEventArgs> ParameterAlarmLimitChanged = (s, a) => { };

        public event EventHandler<AlarmLevelChangedEventArgs> AlarmLevelChanged = (s, a) => { };

        public IPatientMonitorDevice MonitorDevice { get; private set; }

        public Guid UniqueId { get; private set; }

        public bool Subscribe(out long subscriptionId)
        {
            throw new NotImplementedException();
        }

        public bool IsSubscribed
        {
            get { throw new NotImplementedException(); }
        }

        public bool TryDequeue(long subscriptionId, out AbstractPatientMessage message)
        {
            throw new NotImplementedException();
        }

        public void Unsubscribe(long subscriptionId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
