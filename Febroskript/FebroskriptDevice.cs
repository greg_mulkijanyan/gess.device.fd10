﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using Gess.Device.Common;
using Gess.Device.Events;

namespace Gess.Device.Febroskript
{
    public class FebroskriptDevice : IPatientMonitorDevice
    {
        #region [ Constructor(s) ]

        public FebroskriptDevice(ILogger logger)
        {
            if (logger == null) 
                throw new ArgumentNullException("logger");
            
            loggerInstance = logger;
        }

        #endregion

        #region [ Private members ]

        private readonly ILogger loggerInstance;

        private const string RequestHeader = "heckel0<";

        private const string RequestTrailer = ">\r\n";

        private bool endPolling;

        private int interval;

        private long isConnected;

        private int connectTimeout;

        private int sendReceiveTimeout;

        #endregion

        #region [ Protected members ]

        protected virtual void ThreadEntryPoint(object endPoint)
        {
            if (endPoint == null) 
                throw new ArgumentNullException("endPoint");
            
            try
            {
                using (var port = new SerialPort(endPoint.ToString(), 9600, Parity.None, 8, StopBits.One))
                {
                    int calculatedTimeout = 500 * Interval;
                    port.ReadTimeout = port.WriteTimeout = calculatedTimeout;
                    port.NewLine = "\r\n";
                    port.DataReceived += OnSerialPortDataRecevied;
                    port.Open();

                    if (port.BytesToRead > 0)
                    {
                        var buffer = new byte[port.BytesToRead];
                        port.Read(buffer, 0, buffer.Length);
                    }
                    
                    while (!endPolling)
                    {
                        try
                        {
                            string query = BuildQuery(this.ParametersMask);
                            byte[] sendBuffer = Encoding.ASCII.GetBytes(query);
                            port.Write(sendBuffer, 0, sendBuffer.Length);
                            Thread.Sleep(2 * Interval * 1000);
                        }
                        catch (TimeoutException ex)
                        {
                            loggerInstance.LogException(ex);
                            throw;
                        }
                        catch (InvalidOperationException ex)
                        {
                            loggerInstance.LogException(ex);
                            throw;
                        }
                    }

                    port.DataReceived -= OnSerialPortDataRecevied;
                }
            }
            catch (Exception ex)
            {
                loggerInstance.LogException(ex);
                RaiseDisconnected();
            }
        }

        protected void OnSerialPortDataRecevied(object sender, SerialDataReceivedEventArgs args)
        {
            if (sender == null) 
                throw new ArgumentNullException("sender");

            if (args == null) 
                throw new ArgumentNullException("args");

            try
            {
                var port = (SerialPort) sender;
                string dataReceived = port.ReadLine();
                RaiseDataReceived(dataReceived);
            }
            catch (Exception ex)
            {
                loggerInstance.LogException(ex);
                throw;
            }
        }

        protected virtual void RaiseDataReceived(string data)
        {
            var handlers = this.DataReceived;
            if (handlers == null) 
                return;

            if (endPolling) 
                return;

            var handlers2 = handlers.GetInvocationList().Cast<EventHandler<DataReceivedEventArgs>>();
            foreach (var h in handlers2)
            {
                try
                {
                    h.BeginInvoke(this, new DataReceivedEventArgs(new [] {data}), null, null);
                }
                catch (Exception ex)
                {
                    loggerInstance.LogException(ex);
                }
            }
        }

        protected static string BuildQuery(PatientParameter parameterMask)
        {
            var builder = new StringBuilder();

            builder.Append(RequestHeader);
            
            if (0 != (parameterMask & PatientParameter.T1))
            {
                builder.Append("T1;");
            }

            if (0 != (parameterMask & PatientParameter.T2))
            {
                builder.Append("T2;");
            }

            if (0 != (parameterMask & PatientParameter.PR))
            {
                builder.Append("HR;");
            }

            builder.Append(RequestTrailer);
            return builder.ToString();
        }

        protected virtual void RaiseDisconnected()
        {
            var handlers = this.Disconnected;
            if (handlers != null)
            {
                handlers.Invoke(this, new EventArgs());
            }
        }

        #endregion 

        #region [ Public members ]

        public void Connect(DeviceConnectionType connectionType, object endPoint)
        {
            if (connectionType != DeviceConnectionType.Serial) 
                throw new ArgumentException("Only serial connection is supported", "connectionType");

            if (endPoint == null) 
                throw new ArgumentNullException("endPoint");

            if (0 != Interlocked.CompareExchange(ref isConnected, 1, 0))
                throw new InvalidOperationException("Device is already connected.");
            
            try
            {
                var threadStart = new ParameterizedThreadStart(ThreadEntryPoint);
                var thread = new Thread(threadStart);
                thread.Start(endPoint);
            }
            catch (Exception ex)
            {
                loggerInstance.LogException(ex);
                throw;
            }
        }

        public bool TryConnect(DeviceConnectionType connectionType, object endPoint)
        {
            if (connectionType != DeviceConnectionType.Serial)
                throw new ArgumentException("Only serial connection is supported", "connectionType");

            if (endPoint == null)
                throw new ArgumentNullException("endPoint");

            if (0 != Interlocked.Read(ref isConnected)) 
                throw new InvalidOperationException("Device is already connected.");

            try
            {
                using (var port = new SerialPort(endPoint.ToString(), 9600, Parity.None, 8, StopBits.One))
                {
                    port.ReadTimeout = 1000;
                    port.WriteTimeout = 1000;
                    port.Open();
                    var buffer = Encoding.ASCII.GetBytes(BuildQuery(PatientParameter.T1));
                    var receiveBuffer = new byte[port.BytesToRead];
                    port.Write(buffer, 0, buffer.Length);
                    var bytesRead = port.Read(receiveBuffer, 0, receiveBuffer.Length);
                    port.Close();
                    return bytesRead != 0;
                }
            }
            catch (Exception ex)
            {
                loggerInstance.LogException(ex);
                return false;
            }
        }

        public void Disconnect()
        {
            Interlocked.CompareExchange(ref isConnected, 0, 1);
            
            endPolling = true;
        }

        public PatientParameter ParametersMask { get; set; }

        public int Interval
        {
            get
            {
                return interval;
            }
            set
            {
                if (value <= 0) 
                    throw new ArgumentOutOfRangeException("value");
                
                interval = value;
            }
        }

        public event EventHandler<DataReceivedEventArgs> DataReceived = (s, a) => { };

        public event EventHandler Disconnected = (s, a) => { };

        public int ConnectTimeout
        {
            get
            {
                return this.connectTimeout;
            }
            set
            {
                if (value < 0) 
                    throw new ArgumentOutOfRangeException("value");

                this.connectTimeout = value;
            }
        }

        public int SendReceiveTimeout
        {
            get
            {
                return this.sendReceiveTimeout;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("value");

                this.sendReceiveTimeout = value;
            }
        }

        #endregion
    }
}
