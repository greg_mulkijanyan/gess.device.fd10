﻿using System;
using Gess.Device.Common;
using Gess.Device.Events;

namespace Gess.Device
{
    public interface IPatientMonitorDevice
    {
        void Connect(DeviceConnectionType connectionType, object endPoint);

        bool TryConnect(DeviceConnectionType connectionType, object endPoint);

        void Disconnect();

        PatientParameter ParametersMask { get; set; }

        int Interval { get; set; }

        event EventHandler<DataReceivedEventArgs> DataReceived;

        event EventHandler Disconnected;

        int ConnectTimeout { get; set; }

        int SendReceiveTimeout { get; set; }
    }
}
