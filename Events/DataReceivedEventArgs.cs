﻿using System;
using System.Collections.Generic;

namespace Gess.Device.Events
{
    public class DataReceivedEventArgs : EventArgs
    {
        public DataReceivedEventArgs(params string[] data)
        {
            Data = new List<string>(data);
        }

        public DataReceivedEventArgs(IEnumerable<string> data)
        {
            Data = new List<string>(data);
        }

        public List<string> Data { get; private set; }
    }
}
