﻿using System;
using System.Collections.Generic;
using Gess.Device.Common;

namespace Gess.Device.Events
{
    public class AlarmLevelChangedEventArgs : EventArgs
    {
        public AlarmLevelChangedEventArgs(IEnumerable<AlarmLevelChangeInfo> alarmLevelChanges)
        {
            AlarmLevelChanges = new List<AlarmLevelChangeInfo>(alarmLevelChanges);
        }

        public List<AlarmLevelChangeInfo> AlarmLevelChanges { get; private set; }
    }
}
