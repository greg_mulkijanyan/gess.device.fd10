﻿using System;

namespace Gess.Device.Events
{
    public class PhysiologicalAlarmEventArgs : EventArgs
    {
        public int AlarmLevel { get; set; }

        public string AlarmCode { get; set; }

        public string Message { get; set; }
    }
}
