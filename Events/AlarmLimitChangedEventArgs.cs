﻿using System;
using System.Collections.Generic;
using Gess.Device.Common;

namespace Gess.Device.Events
{
    public class AlarmLimitChangedEventArgs : EventArgs
    {
        public AlarmLimitChangedEventArgs(IEnumerable<AlarmLimitChangeInfo> alarmLimitsChanged)
        {
            AlarmLimitsChanged = new List<AlarmLimitChangeInfo>(alarmLimitsChanged);
        }

        public List<AlarmLimitChangeInfo> AlarmLimitsChanged { get; private set; }
    }
}
