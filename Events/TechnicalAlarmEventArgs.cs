﻿using System;

namespace Gess.Device.Events
{
    public class TechnicalAlarmEventArgs : EventArgs
    {
        public int AlarmLevel { get; set; }

        public int AlarmId { get; set; }
        
        public string Message { get; set; }

        public string Source { get; set; }
    }
}
