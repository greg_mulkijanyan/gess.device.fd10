﻿using System;
using System.Collections.Generic;
using Gess.Device.Common;
using Gess.Device.Events;
using Gess.Device.Messages;

namespace Gess.Device
{
    /// <summary>
    /// 
    /// </summary>
    // ReSharper disable InconsistentNaming
    public interface IPatientDataSource
    {
        /// <summary>
        /// Gets Temperature 1 value.
        /// </summary>
        /// <returns></returns>
        double GetT1();

        /// <summary>
        /// Gets Temperature 2 value.
        /// </summary>
        /// <returns></returns>
        double GetT2();

        /// <summary>
        /// Gets T delta.
        /// </summary>
        /// <returns></returns>
        double GetTD();

        /// <summary>
        /// Gets Systolic Non-Invasive Blood Pressure parameter.
        /// </summary>
        /// <returns></returns>
        double Get_ECG_NIBPSis();

        /// <summary>
        /// Gets Diastolic Non-Invasive Blood Pressure parameter.
        /// </summary>
        /// <returns></returns>
        double Get_ECG_NIBPDia();

        /// <summary>
        /// Gets Mean Non-Invasive Blood Pressure parameter.
        /// </summary>
        /// <returns></returns>
        double Get_ECG_NIBPMean();

        /// <summary>
        /// Gets Pulse Rate parameter via SpO2 module.
        /// </summary>
        /// <returns></returns>
        double Get_SpO2_PR();

        /// <summary>
        /// Gets SpO2 parameter.
        /// </summary>
        /// <returns></returns>
        double Get_SpO2();

        /// <summary>
        /// Gets Respiration Rate parameter via ECG module.
        /// </summary>
        /// <returns></returns>
        double Get_ECG_RR();

        /// <summary>
        /// Gets cardiac output
        /// </summary>
        /// <returns></returns>
        double Get_CO();

        
        /// <summary>
        /// Gets CO2 concentration.
        /// </summary>
        /// <returns></returns>
        double Get_CO2();

        /// <summary>
        /// Checks if implementation supports certain patient parameter.
        /// </summary>
        /// <param name="patientParameter"></param>
        /// <returns></returns>
        bool Supports(PatientParameter patientParameter);

        /// <summary>
        /// Gets measurement units for patient parameters.
        /// </summary>
        /// <returns></returns>
        Dictionary<PatientParameter, string> GetMeasurementUnits();

        /// <summary>
        /// Tries to get value
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool TryGetValue(PatientParameter parameter, out double value);

        /// <summary>
        /// Gets upper limit for specified patient parameter.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        double GetUpperLimit(PatientParameter value);

        /// <summary>
        /// Gets lower limit for specified patient parameter.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        double GetLowerLimit(PatientParameter value);

        /// <summary>
        /// Resets all previously received patient parameters.
        /// </summary>
        void Reset();

        /// <summary>
        /// Raised when patient data is received.
        /// </summary>
        event EventHandler<PatientStateData> PatientDataReceived;

        /// <summary>
        /// Raised when alarm is raised because of technical condition.
        /// </summary>
        event EventHandler<TechnicalAlarmEventArgs> TechnicalAlarm;

        /// <summary>
        /// Raised when one or more patient physiological parameters are out of given range.
        /// </summary>
        event EventHandler<PhysiologicalAlarmEventArgs> PhysiologicalAlarm;

        /// <summary>
        /// Raised when patient parameter's limits have changed.
        /// </summary>
        event EventHandler<AlarmLimitChangedEventArgs> ParameterAlarmLimitChanged;

        /// <summary>
        /// Raised when alarm level is changed for certain parameters.
        /// </summary>
        event EventHandler<AlarmLevelChangedEventArgs> AlarmLevelChanged;

        /// <summary>
        /// Gets underlying device connector component.
        /// </summary>
        IPatientMonitorDevice MonitorDevice { get; }

        /// <summary>
        /// Gets unique ID for this instance.
        /// </summary>
        Guid UniqueId { get; }

        /// <summary>
        /// Subscribe to message queue, if no-one is subscribed yet.
        /// </summary>
        /// <param name="subscriptionId">Subscription ID assigned.</param>
        /// <returns>True, if subscription was successful, false otherwise.</returns>
        bool Subscribe(out long subscriptionId);

        /// <summary>
        /// Gets the subscription status.
        /// </summary>
        bool IsSubscribed { get; }

        /// <summary>
        /// Tries to dequeue the next message.
        /// </summary>
        /// <param name="subscriptionId">Subscription ID.</param>
        /// <param name="message">Message object dequeued.</param>
        /// <returns>True if successful, false otherwise.</returns>
        bool TryDequeue(long subscriptionId, out AbstractPatientMessage message);

        void Unsubscribe(long subscriptionId);
    }
    // ReSharper restore InconsistentNaming
}
